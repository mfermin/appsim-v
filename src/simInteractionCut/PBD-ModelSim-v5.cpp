/*
** This code based in library: iMSTK, nlohmann_json, Vrpn
** The iMSTK is distributed under the Apache License, Version 2.0.
** The Nlohmann_json library is licensed under the MIT License.
** The VRPN library is licensed under the Boost Software License 1.0
** for more details see Readme.md.
*/
#include "CutHelp.h"
#include "imstkNew.h"
#include "imstkCamera.h"
#include "imstkDeviceManager.h"
#include "imstkDeviceManagerFactory.h"
#include "imstkDirectionalLight.h"
#include "imstkGeometryUtilities.h"
#include "imstkKeyboardDeviceClient.h"
#include "imstkKeyboardSceneControl.h"
#include "imstkMouseDeviceClient.h"
#include "imstkMouseSceneControl.h"
#include "imstkPbdModel.h"
#include "imstkPbdModelConfig.h"
#include "imstkPbdObject.h"
#include "imstkRigidBodyModel2.h"
#include "imstkRigidObject2.h"
#include "imstkPbdObjectCollision.h"
#include "imstkPbdObjectController.h"
#include "imstkPlane.h"
#include "imstkPointwiseMap.h"
#include "imstkRenderMaterial.h"
#include "imstkScene.h"
#include "imstkSceneManager.h"
#include "imstkSimulationManager.h"
#include "imstkSimulationUtils.h"
#include "imstkVisualModel.h"
#include "imstkVTKViewer.h"
#include "imstkRigidObjectController.h"

#include "imstkOrientedBox.h"

#include "imstkPbdRigidBaryPointToPointConstraint.h"
#include "imstkPbdRigidObjectGrasping.h"
#include "imstkPbdRigidObjectGrasping.h"
#include "imstkPbdObjectCutting.h"

//mesh
#include "imstkTetrahedralMesh.h"
#include "imstkVTKMeshIO.h"

//#interaccion control
#include "imstkKeyboardDeviceClient.h"
#include "imstkKeyboardSceneControl.h"
#include "imstkMouseDeviceClient.h"
#include "imstkMouseSceneControl.h"
//file to config
#include "nlohmann/json.hpp"
#include <vector>

using namespace imstk;
using namespace std;
using json = nlohmann::json;

std::shared_ptr<PbdObject> makePBD_Object(const std::string& fname);


static void
addDummyVertexPointSet(std::shared_ptr<PointSet> pointSet)
{
    // Add a dummy vertex to the vertices
    std::shared_ptr<VecDataArray<double, 3>> verticesPtr = pointSet->getVertexPositions();
    VecDataArray<double, 3>&                 vertices    = *verticesPtr;
    vertices.resize(vertices.size() + 1);
    for (int i = vertices.size() - 1; i >= 1; i--)
    {
        vertices[i] = vertices[i - 1];
    }
    vertices[0] = Vec3d(0.0, 0.0, 0.0);
    pointSet->setInitialVertexPositions(std::make_shared<VecDataArray<double, 3>>(*verticesPtr));
}

static void
addDummyVertex(std::shared_ptr<SurfaceMesh> surfMesh)
{
    addDummyVertexPointSet(surfMesh);

    // Then shift all indices by 1
    std::shared_ptr<VecDataArray<int, 3>> indicesPtr = surfMesh->getTriangleIndices();
    VecDataArray<int, 3>&                 indices    = *indicesPtr;
    for (int i = 0; i < indices.size(); i++)
    {
        indices[i][0]++;
        indices[i][1]++;
        indices[i][2]++;
    }
}

static void
addDummyVertex(std::shared_ptr<TetrahedralMesh> tetMesh)
{
    addDummyVertexPointSet(tetMesh);

    // Then shift all indices by 1
    std::shared_ptr<VecDataArray<int, 4>> tissueIndicesPtr = tetMesh->getTetrahedraIndices();
    VecDataArray<int, 4>&                 tissueIndices    = *tissueIndicesPtr;
    for (int i = 0; i < tissueIndices.size(); i++)
    {
        tissueIndices[i][0]++;
        tissueIndices[i][1]++;
        tissueIndices[i][2]++;
        tissueIndices[i][3]++;
    }
}

///
/// \brief Creates a tetraheral grid
/// \param physical dimension of tissue
/// \param dimensions of tetrahedral grid used for tissue
/// \param center of grid
///
static std::shared_ptr<TetrahedralMesh>
makeTetGrid(const Vec3d& size, const Vec3i& dim, const Vec3d& center)
{
    imstkNew<TetrahedralMesh> tissueMesh;

    imstkNew<VecDataArray<double, 3>> verticesPtr(dim[0] * dim[1] * dim[2]);
    VecDataArray<double, 3>&          vertices = *verticesPtr.get();
    const Vec3d                       dx       = size.cwiseQuotient((dim - Vec3i(1, 1, 1)).cast<double>());
    for (int z = 0; z < dim[2]; z++)
    {
        for (int y = 0; y < dim[1]; y++)
        {
            for (int x = 0; x < dim[0]; x++)
            {
                vertices[x + dim[0] * (y + dim[1] * z)] = Vec3i(x, y, z).cast<double>().cwiseProduct(dx) - size * 0.5 + center;
            }
        }
    }

    // Add connectivity data
    imstkNew<VecDataArray<int, 4>> indicesPtr;
    VecDataArray<int, 4>&          indices = *indicesPtr.get();
    for (int z = 0; z < dim[2] - 1; z++)
    {
        for (int y = 0; y < dim[1] - 1; y++)
        {
            for (int x = 0; x < dim[0] - 1; x++)
            {
                int cubeIndices[8] =
                {
                    x + dim[0] * (y + dim[1] * z),
                    (x + 1) + dim[0] * (y + dim[1] * z),
                    (x + 1) + dim[0] * (y + dim[1] * (z + 1)),
                    x + dim[0] * (y + dim[1] * (z + 1)),
                    x + dim[0] * ((y + 1) + dim[1] * z),
                    (x + 1) + dim[0] * ((y + 1) + dim[1] * z),
                    (x + 1) + dim[0] * ((y + 1) + dim[1] * (z + 1)),
                    x + dim[0] * ((y + 1) + dim[1] * (z + 1))
                };

                // Alternate the pattern so the edges line up on the sides of each voxel
                if ((z % 2 ^ x % 2) ^ y % 2)
                {
                    indices.push_back(Vec4i(cubeIndices[0], cubeIndices[7], cubeIndices[5], cubeIndices[4]));
                    indices.push_back(Vec4i(cubeIndices[3], cubeIndices[7], cubeIndices[2], cubeIndices[0]));
                    indices.push_back(Vec4i(cubeIndices[2], cubeIndices[7], cubeIndices[5], cubeIndices[0]));
                    indices.push_back(Vec4i(cubeIndices[1], cubeIndices[2], cubeIndices[0], cubeIndices[5]));
                    indices.push_back(Vec4i(cubeIndices[2], cubeIndices[6], cubeIndices[7], cubeIndices[5]));
                }
                else
                {
                    indices.push_back(Vec4i(cubeIndices[3], cubeIndices[7], cubeIndices[6], cubeIndices[4]));
                    indices.push_back(Vec4i(cubeIndices[1], cubeIndices[3], cubeIndices[6], cubeIndices[4]));
                    indices.push_back(Vec4i(cubeIndices[3], cubeIndices[6], cubeIndices[2], cubeIndices[1]));
                    indices.push_back(Vec4i(cubeIndices[1], cubeIndices[6], cubeIndices[5], cubeIndices[4]));
                    indices.push_back(Vec4i(cubeIndices[0], cubeIndices[3], cubeIndices[1], cubeIndices[4]));
                }
            }
        }
    }

    // Ensure correct windings
    for (int i = 0; i < indices.size(); i++)
    {
        if (tetVolume(vertices[indices[i][0]], vertices[indices[i][1]], vertices[indices[i][2]], vertices[indices[i][3]]) > 0.0)
        {
            std::swap(indices[i][0], indices[i][2]);
        }
    }

    tissueMesh->initialize(verticesPtr, indicesPtr);

    return tissueMesh;
}

///
/// \brief Creates tissue object
/// \param name
/// \param physical dimension of tissue
/// \param dimensions of tetrahedral grid used for tissue
/// \param center of tissue block
///
static std::shared_ptr<PbdObject>
makeTissueObj(const std::string& name,
              const Vec3d& size, const Vec3i& dim, const Vec3d& center,
              std::shared_ptr<PbdModel> model)
{
    // Setup the Geometry
    std::shared_ptr<TetrahedralMesh> tissueMesh = GeometryUtils::toTetGrid(center, size, dim);
    std::shared_ptr<SurfaceMesh>     surfMesh   = tissueMesh->extractSurfaceMesh();

    addDummyVertex(tissueMesh);
    //addDummyVertex(surfMesh);

    // Add a mask of ints to denote how many elements are referencing this vertex
    auto referenceCountPtr = std::make_shared<DataArray<int>>(tissueMesh->getNumVertices());
    referenceCountPtr->fill(0);
    tissueMesh->setVertexAttribute("ReferenceCount", referenceCountPtr);

    // Use FEMTet constraints
    model->getConfig()->m_femParams->m_YoungModulus = 50.0;
    model->getConfig()->m_femParams->m_PoissonRatio = 0.4;
    model->getConfig()->enableFemConstraint(PbdFemConstraint::MaterialType::StVK);

    // Setup the material
    auto material = std::make_shared<RenderMaterial>();
    material->setBackFaceCulling(false);
    material->setDisplayMode(RenderMaterial::DisplayMode::WireframeSurface);
    material->setShadingModel(RenderMaterial::ShadingModel::PBR);

    // Setup the Object
    auto tissueObj = std::make_shared<PbdObject>(name);
    tissueObj->setPhysicsGeometry(tissueMesh);
    tissueObj->setVisualGeometry(tissueMesh);
    tissueObj->setCollidingGeometry(surfMesh);
    auto map = std::make_shared<PointwiseMap>(tissueMesh, surfMesh);
    tissueObj->setPhysicsToCollidingMap(map);
    tissueObj->getVisualModel(0)->setRenderMaterial(material);
    tissueObj->setDynamicalModel(model);
    tissueObj->getPbdBody()->uniformMassValue = 0.1;
    // Fix the borders
    for (int z = 0; z < dim[2]; z++)
    {
        for (int y = 0; y < dim[1]; y++)
        {
            for (int x = 0; x < dim[0]; x++)
            {
                if (x == 0 || /*z == 0 ||*/ x == dim[0] - 1 /*|| z == dim[2] - 1*/)
                {
                    tissueObj->getPbdBody()->fixedNodeIds.push_back(x + dim[0] * (y + dim[1] * z) + 1); // +1 for dummy vertex
                }
            }
        }
    }
    tissueObj->getPbdBody()->fixedNodeIds.push_back(0); // Fix dummy vertex

    return tissueObj;
}

/*static std::shared_ptr<RigidObject2>
makeToolObj()
{
    imstkNew<Plane> toolGeometry(Vec3d(0.0, 1.0, 0.0));
    toolGeometry->setWidth(1.0);

    imstkNew<RigidObject2> toolObj("Tool");
    toolObj->setVisualGeometry(toolGeometry);
    toolObj->setCollidingGeometry(toolGeometry);
    toolObj->setPhysicsGeometry(toolGeometry);
    toolObj->getVisualModel(0)->getRenderMaterial()->setColor(Color::Blue);
    toolObj->getVisualModel(0)->getRenderMaterial()->setDisplayMode(RenderMaterial::DisplayMode::WireframeSurface);
    toolObj->getVisualModel(0)->getRenderMaterial()->setBackFaceCulling(false);
    toolObj->getVisualModel(0)->getRenderMaterial()->setLineWidth(1.0);

    std::shared_ptr<RigidBodyModel2> rbdModel = std::make_shared<RigidBodyModel2>();
    rbdModel->getConfig()->m_gravity =Vec3d::Zero();
    rbdModel->getConfig()->m_maxNumIterations       = 8;
    rbdModel->getConfig()->m_velocityDamping        = 1.0;
    rbdModel->getConfig()->m_angularVelocityDamping = 1.0;
    rbdModel->getConfig()->m_maxNumConstraints      = 40;
    toolObj->setDynamicalModel(rbdModel);

    toolObj->getRigidBody()->m_mass = 1.0;
    toolObj->getRigidBody()->m_intertiaTensor = 
    Mat3d::Identity() * 10000.0;
    toolObj->getRigidBody()->m_initPos = Vec3d(0.0, 0.0, 0.0);

    return toolObj;
}*/

static std::shared_ptr<RigidObject2>
makeToolObj()
{
    // Set up rigid body model
    std::shared_ptr<RigidBodyModel2> rbdModel = std::make_shared<RigidBodyModel2>();
    rbdModel->getConfig()->m_gravity = Vec3d::Zero();
    rbdModel->getConfig()->m_maxNumIterations       = 8;
    rbdModel->getConfig()->m_velocityDamping        = 1.0;
    rbdModel->getConfig()->m_angularVelocityDamping = 0.0;// 1.0;
    rbdModel->getConfig()->m_maxNumConstraints      = 100;
    //rbdModel->getConfig()->m_uniformMassValue= 1.0
    
    //auto toolGeometry = std::make_shared<Capsule>();
    auto toolGeometry = std::make_shared<OrientedBox>();
    //toolGeometry->setRadius(0.5);
    //toolGeometry->setLength(1);
    toolGeometry->setExtents(1.0,1.0,0.6    );
    toolGeometry->setPosition(Vec3d(0.5, 1.0, 0.5)); //move Tool
    toolGeometry->setOrientation(Quatd(0.707,0.707 , 0.0, 0.0));
    //toolGeometry->setOrientation(Quatd(0.707,0.0 , 0.0, 0.707));

    std::shared_ptr<RigidObject2> toolObj = std::make_shared<RigidObject2>("Tool");

    // Create the object
    toolObj->setVisualGeometry(toolGeometry);
    toolObj->setPhysicsGeometry(toolGeometry);
    toolObj->setCollidingGeometry(toolGeometry);
    toolObj->setDynamicalModel(rbdModel);
    toolObj->getRigidBody()->m_mass = 1.0;
    //toolObj->getRigidBody()->m_intertiaTensor = Mat3d::Identity() * 1.0;
    //toolObj->getRigidBody()->m_initPos = Vec3d(0.0, 0.0, 0.0);
    //toolObj->getRigidBody()->m_initPos = Vec3d(1.0, 0.0, 2.0); //cube 5x5x5

    toolObj->getVisualModel(0)->getRenderMaterial()->setOpacity(0.5);

    return toolObj;
}

const std::string readFileJson(const std::string& fname, const imstkNew<PbdModelConfig>& confPBD)  
{  
    //imstkNew<FemModelConfig> config;

    //json root = "{ \"E\": 1E7, \"nu\": 0.4, \"gravity\": 9.8, \"fixPnt\":[91, 92, 93, 94, 95]}"_json;
    //std::ofstream file(fname);
    //file << root;
    //file << std::setw(4) << root << std::endl;
   
    // read from the file
    ifstream in(fname, ios::binary);  

    if( !in.is_open() )    
    {   
    cout << "Error opening configFile\n";   
    return 0;   
    }
    else{
        cout << "Opening configFile\n"; 
        json  jsonread= json::parse(in);
        //LOG(INFO)<<"log Fix";
        for (int i =0; i<jsonread["fixPnt"].size(); i++){
            //LOG(INFO)<< jsonread["fixPnt"][i].get<int>();
            confPBD->m_fixedNodeIds.push_back(jsonread["fixPnt"][i].get<int>());
        }
        //
        LOG(INFO)<<"log Ext";
        for (int i =0; i<jsonread["FExtPnt"].size(); i++){
            LOG(INFO)<< jsonread["FExtPnt"][i].get<int>();
            confPBD->m_fExtNodeIds.push_back(jsonread["FExtPnt"][i].get<int>());
        }
        confPBD->m_fext = Vec3d(jsonread["fext"][0].get<double>(), jsonread["fext"][1].get<double>(), jsonread["fext"][2].get<double>());
        //f_ctrl = Vec3d(jsonread["fctrl"][0].get<double>(), jsonread["fctrl"][1].get<double>(), jsonread["fctrl"][2].get<double>());
        LOG(INFO)<<"fext: "<< confPBD->m_fext;
        //confFem->m_density = jsonread["density"].get<double>();
        //confFem->m_fixedDOFFilename= std::string
        //std::vector<std::size_t> m_fixedNodeIds;
        //auto fixed_pointed= jsonread["fixPnt"].get<std::vector<int>>(); //std::vector<std::size_t> fixedNodeIds;
        //LOG(INFO)<< fixed_pointed;

        confPBD->m_iterations = jsonread["iteration"].get<double>();;
        confPBD->m_linearDampingCoeff= jsonread["damping"].get<double>();;
        //confPBD->m_uniformMassValue = jsonread["mass"].get<double>(); //dimension of fext vector

        confPBD->m_femParams->m_YoungModulus = jsonread["E"].get<double>();;//1000.0;//1000;//350.0; 1E9 //1E8
        confPBD->m_femParams->m_PoissonRatio = jsonread["nu"].get<double>();;//0.3;//0.45;
        //confPBD->m_femParams->m_mu = jsonread["mu"].get<double>(); //paper model: 10
        //confPBD->m_femParams->m_lambda = jsonread["lambda"].get<double>(); //paper : 1

        

        //vertexIdx = jsonread["vertexIdx"].get<int>();
        //confPBD->m_FemParams->m_mu = 384615;
        //confPBD->m_FemParams->m_lambda = 576923;

        //pbdParams->m_fixedNodeIds = { 1,2,5,6 };
        //confPBD->m_fixedNodeIds = fixed_pointed;
       /* confPBD->m_fixedNodeIds = {
        //BEAM 
        //+cara superior
        //91, 92, 93, 94, 95, 186, 187, 188, 189, 190,  281, 282, 283, 284, 285,  376, 377, 378, 379, 380,  471, 472, 473, 474, 475, //edge sup        
        //+fila 1 (superior)
        //90, 185,280,375,470, 469,468,467,466,371, 276,181,86,87,88,89 
        //+cara inferior:
        //1,2,3,4,5, 96,97,98,99,100, 191,192,193,194,195, 286,287,288,289,290, 381,382,383,384,385
        //CUBE
        //(z=0): 0,1,2,3, 24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39, 106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130
        //(Y=0):
        0,1,4,5, 16,17,18,19, 28,29,30,31, 44,45,46,47, 52,53,54,55, 131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155
        };*/
        string materialHyEl= jsonread["HyperElastMat"].get<string>();

        if (materialHyEl == "StVK") confPBD->enableFemConstraint(PbdFemConstraint::MaterialType::StVK);
        else if (materialHyEl == "NeoHookean") confPBD->enableFemConstraint(PbdFemConstraint::MaterialType::NeoHookean);
        else if (materialHyEl == "Corotation") confPBD->enableFemConstraint(PbdFemConstraint::MaterialType::Corotation);
        else if (materialHyEl == "StNeoHookean") confPBD->enableFemConstraint(PbdFemConstraint::MaterialType::StNeoHookean);
        else if (materialHyEl == "StableNeoHookean") confPBD->enableFemConstraint(PbdFemConstraint::MaterialType::StableNeoHookean);
        else if (materialHyEl == "MooneyRivlin"){ 
            confPBD->enableFemConstraint(PbdFemConstraint::MaterialType::MooneyRivlin);
            confPBD->m_femParams->m_mu10= jsonread["mu10"].get<double>();;
            confPBD->m_femParams->m_mu01= jsonread["mu01"].get<double>();;
            confPBD->m_femParams->m_v1= jsonread["v1"].get<double>();;
        
        }else if (materialHyEl == "MooneyRivlin5") confPBD->enableFemConstraint(PbdFemConstraint::MaterialType::MooneyRivlin5);
        else if (materialHyEl == "Linear") confPBD->enableFemConstraint(PbdFemConstraint::MaterialType::Linear);
        

        LOG(INFO)<< "Material: "<< materialHyEl;
        //setParticleMass

       
        //confPBD->enableConstraint(PbdConstraint::Type::FemTet, confPBD->m_FemParams->m_YoungModulus);
        //confPBD->m_gravity    = Vec3d(0, -jsonread["gravity"].get<double>(), 0);  
        confPBD->m_gravity = Vec3d(jsonread["gravity"][0].get<double>(), jsonread["gravity"][1].get<double>(), jsonread["gravity"][2].get<double>());
        //confPBD->m_gravity = Vec3d(jsonread["Position"][0].get<double>(), jsonread["Position"][1].get<double>(), jsonread["Position"][2].get<double>());

        //confPBD->m_dt  = 0.00;//0.01;//0.01;//0.008;
        //confPBD->collisionParams->m_proximity = 0.3;
        //confPBD->collisionParams->m_stiffness = 0.01;

        confPBD->m_doPartitioning   = false;
        //confPBD->m_uniformMassValue = 0.1;
        //confPBD->m_gravity    = Vec3d(0.0, -0.2, 0.0);
        confPBD->m_dt         = jsonread["dt"].get<double>();;

        if(DEBUG_ON)LOG(INFO)<<"READ FILE OPEN"<< iMSTK_DATA_ROOT+jsonread["meshFile"].get<string>();
        return iMSTK_DATA_ROOT+jsonread["meshFile"].get<string>();
    }
}


///
/// \brief This example demonstrates tetrahedral removal of a pbd simulated mesh
/// using a haptic device. Hold the button the device whilst moving it over elements
/// to remove
///
int
main(int argc, char *argv[])
{
    Logger::startLogger();
    
    /// \ IP address of the server.

    if (argc != 2)
    {
        LOG(FATAL) << "Usage:" << argv[0] << " [file Config] .json";
        return EXIT_FAILURE;

    }
    const std::string& configFileJson = std::string(argv[1]);

    // Setup logger (write to file and stdout)
    Logger::startLogger();

    // Setup the scene
    imstkNew<Scene> scene("PBDTissueCut");
    scene->getActiveCamera()->setPosition(0.12, 4.51, 16.51);
    scene->getActiveCamera()->setFocalPoint(0.0, 0.0, 0.0);
    scene->getActiveCamera()->setViewUp(0.0, 0.96, -0.28);


    auto pbdModel = std::make_shared<PbdModel>();
    pbdModel->getConfig()->m_doPartitioning = false;
    pbdModel->getConfig()->m_gravity    = Vec3d(0.0, -0.2, 0.0);
    pbdModel->getConfig()->m_dt         = 0.05;
    pbdModel->getConfig()->m_iterations = 5;

    // Setup a tissue
      std::shared_ptr<PbdObject> tissueObj = makeTissueObj("Tissue",
       Vec3d(10.0, 3.0, 10.0), Vec3i(10, 3, 10), Vec3d(0.0, -1.0, 0.0),pbdModel);


   // std::shared_ptr<PbdObject> tissueObj = makePBD_Object(configFileJson);

    scene->addSceneObject(tissueObj);

    std::shared_ptr<RigidObject2> toolObj = makeToolObj();
    scene->addSceneObject(toolObj);

    //auto cutting = std::make_shared<PbdObjectCutting>(tissueObj, toolObj);
    //scene->addInteraction(cutting);

    /*auto interaction = std::make_shared<PbdObjectCollision>(tissueObj, toolObj, "MeshToMeshBruteForceCD");
    scene->addInteraction(interaction);*/

    // Light
    imstkNew<DirectionalLight> light;
    light->setFocalPoint(Vec3d(5.0, -8.0, -5.0));
    light->setIntensity(1.0);
    scene->addLight("Light", light);

    // Run the simulation
    {
        // Setup a viewer to render
        imstkNew<VTKViewer> viewer;
        viewer->setActiveScene(scene);
        viewer->setVtkLoggerMode(VTKViewer::VTKLoggerMode::MUTE);

        // Setup a scene manager to advance the scene
        imstkNew<SceneManager> sceneManager;
        sceneManager->setActiveScene(scene);
        sceneManager->pause(); // Start simulation paused

        imstkNew<SimulationManager> driver;
        driver->addModule(viewer);
        driver->addModule(sceneManager);
        driver->setDesiredDt(0.01);

        /*imstkNew<HapticDeviceManager> hapticManager;
        hapticManager->setSleepDelay(1.0); // Delay for 1ms (haptics thread is limited to max 1000hz)
        std::shared_ptr<HapticDeviceClient> hapticDeviceClient = hapticManager->makeDeviceClient();
        driver->addModule(hapticManager);*/

        /*imstkNew<RigidObjectController> controller(toolObj, hapticDeviceClient);
        controller->setTranslationScaling(0.06);
        controller->setLinearKs(1000.0);
        controller->setLinearKd(50.0);
        controller->setAngularKs(10000000.0);
        controller->setAngularKd(500000.0);
        controller->setForceScaling(0.001);
        scene->addController(controller);*/

        /*connect<Event>(sceneManager, &SceneManager::postUpdate, [&](Event*)
        {
            const Vec2d mousePos   = viewer->getMouseDevice()->getPos();
            const Vec3d desiredPos = Vec3d(mousePos[0] - 0.5, mousePos[1] - 0.5, 0.0) * 2.0 + Vec3d(0.0, 1.0, 0.0);
            const Quatd desiredOrientation = Quatd(Rotd(0.0, Vec3d(1.0, 0.0, 0.0)));

            Vec3d virtualForce;
            {
                const Vec3d fS = (desiredPos - toolObj->getRigidBody()->getPosition()) * 1000.0;   // Spring force
                const Vec3d fD = -toolObj->getRigidBody()->getVelocity() * 100.0;                  // Spring damping

                const Quatd dq       = desiredOrientation * toolObj->getRigidBody()->getOrientation().inverse();
                const Rotd angleAxes = Rotd(dq);
                const Vec3d tS       = angleAxes.axis() * angleAxes.angle() * 10000000.0;
                const Vec3d tD       = -toolObj->getRigidBody()->getAngularVelocity() * 1000.0;

                virtualForce = fS + fD;
                (*toolObj->getRigidBody()->m_force)  += virtualForce;
                (*toolObj->getRigidBody()->m_torque) += tS + tD;
            }

            // Update the virual gosh debug geometry
            /*std::shared_ptr<Geometry> toolGhostMesh = toolPushVirtual->getVisualGeometry();
            toolGhostMesh->setRotation(desiredOrientation);
            toolGhostMesh->setTranslation(desiredPos);
            toolGhostMesh->updatePostTransformData();
            toolGhostMesh->postModified();

            toolPushVirtual->getVisualModel(0)->getRenderMaterial()->setOpacity(std::min(1.0, virtualForce.norm() / 15.0));
            });*/

        connect<Event>(sceneManager, &SceneManager::postUpdate, [&](Event*)
            {
                //toolObj->getRigidBodyModel2()->getConfig()->m_dt = sceneManager->getDt();
                pbdModel->getConfig()->m_dt = sceneManager->getDt();
                //tissueObj->getLevelSetModel()->getConfig()->m_dt = sceneManager->getDt();

                const Vec2d mousePos = viewer->getMouseDevice()->getPos();
                const Vec3d worldPos = Vec3d(mousePos[0]-2 , mousePos[1] , -1);

                const Vec3d fS = (worldPos - toolObj->getRigidBody()->getPosition()) * 1000.0;     // Spring force
                const Vec3d fD = -toolObj->getRigidBody()->getVelocity() * 100.0;                  // Spring damping

                (*toolObj->getRigidBody()->m_force) += (fS + fD);

                // Also apply controller transform to ghost geometry
                /*ghostMesh->setTranslation(worldPos);
                ghostMesh->setRotation(Mat3d::Identity());
                ghostMesh->updatePostTransformData();
                ghostMesh->postModified();*/
            });


        ////////////////////////


        connect<KeyEvent>(viewer->getKeyboardDevice(), &KeyboardDeviceClient::keyPress, [&](KeyEvent* e)
            {
               //toolObj->getRigidBodyModel2()->getConfig()->m_dt = sceneManager->getDt();
                if (e->m_key == 'i')
                {
                    auto tissueMesh = std::dynamic_pointer_cast<TetrahedralMesh>(tissueObj->getPhysicsGeometry());
                    auto toolGeom   = std::dynamic_pointer_cast<SurfaceMesh>(toolObj->getCollidingGeometry());

                    // Default config of the tool is pointing downwards on y
                    const Mat3d rot     = toolGeom->getRotation();
                    const Vec3d forward = (rot * Vec3d(0.0, 0.0, 1.0)).normalized();
                    const Vec3d left    = (rot * Vec3d(1.0, 0.0, 0.0)).normalized();
                    const Vec3d n       = (rot * Vec3d(0.0, 1.0, 0.0)).normalized();

                    const Vec3d planePos        = toolGeom->getTranslation();
                    const Vec3d planeNormal     = n;
                    const double planeWidth     = 1.1; // Slightly larger than collision geometry
                    const double planeHalfWidth = planeWidth * 0.5;

                    std::shared_ptr<VecDataArray<double, 3>> tissueVerticesPtr = tissueMesh->getVertexPositions();
                    std::shared_ptr<VecDataArray<int, 4>> tissueIndicesPtr     = tissueMesh->getCells();
                    VecDataArray<double, 3>& tissueVertices = *tissueVerticesPtr;
                    VecDataArray<int, 4>& tissueIndices     = *tissueIndicesPtr;

                    // Compute which tets should be removed
                    std::unordered_set<int> removedTets;
                    for (int i = 0; i < tissueIndices.size(); i++)
                    {
                        Vec4i& tet = tissueIndices[i];
                        std::array<Vec3d, 4> tetVerts;
                        tetVerts[0] = tissueVertices[tet[0]];
                        tetVerts[1] = tissueVertices[tet[1]];
                        tetVerts[2] = tissueVertices[tet[2]];
                        tetVerts[3] = tissueVertices[tet[3]];

                        if (splitTest(tetVerts, planePos, left, planeHalfWidth, forward, planeHalfWidth, n))
                        {
                            // Remove the tet being split
                            removedTets.insert(i);
                        }
                    }

                    // Deal with diffs
                    std::shared_ptr<PbdConstraintContainer> constraintsPtr = tissueObj->getPbdModel()->getConstraints();
                    const std::vector<std::shared_ptr<PbdConstraint>>& constraints = constraintsPtr->getConstraints();

                    // First process all removed tets by removing the constraints and setting the element to the dummy vertex
                    for (auto i : removedTets)
                    {
                        Vec4i& tet = tissueIndices[i];

                        // Find and remove the associated constraints
                        for (auto j = constraints.begin(); j != constraints.end(); j++)
                        {
                            const std::vector<PbdParticleId>& vertexIds = (*j)->getParticles();
                            bool isSameTet = true;
                            for (int k = 0; k < 4; k++)
                            {
                                if (vertexIds[k].second != tet[k])
                                {
                                    isSameTet = false;
                                    break;
                                }
                            }
                            if (isSameTet)
                            {
                                constraintsPtr->eraseConstraint(j);
                                break;
                            }
                        }

                        // Set removed tet to dummy vertex
                        tet = Vec4i(0, 0, 0, 0);
                    }

                    if (removedTets.size() > 0)
                    {
                        //// Update collision geometry by re-extracting the entire mesh
                        //auto map = std::dynamic_pointer_cast<PointwiseMap>(tissueObj->getPhysicsToCollidingMap());
                        //std::shared_ptr<SurfaceMesh> colMesh = tissueMesh->extractSurfaceMesh();
                        //map->setChildGeometry(colMesh);
                        //map->compute();
                        //map->update();
                        //colMesh->computeVertexNormals();
                        //tissueObj->setCollidingGeometry(colMesh);

                        tissueIndicesPtr->postModified();
                        tissueMesh->postModified();
                    }
                }
        });

        // Add mouse and keyboard controls to the viewer
        {
        
        std::shared_ptr<Entity> mouseAndKeyControls =
            SimulationUtils::createDefaultSceneControl(driver);
        //mouseAndKeyControls->addComponent(statusText);
        scene->addSceneObject(mouseAndKeyControls);
        }

        driver->start();
    }

    return 0;
}

/* 
    Functon to generate pbd object 
 */
std::shared_ptr<PbdObject>
makePBD_Object( const std::string& fname)
{
    // Create the PBD object
    imstkNew<PbdObject> pbdObj("ObjectPbd");
    imstkNew<PbdModelConfig> pbdParams;
    
    //Reading file & config parameters pbd
    std::shared_ptr<TetrahedralMesh> tissueMesh = MeshIO::read<TetrahedralMesh>(readFileJson(fname, pbdParams));

    //surfMesh = MeshIO::read<SurfaceMesh>

    CHECK(tissueMesh != nullptr) << "Could not read mesh from file.";

    //addDummyVertex(tissueMesh);

    // Add a mask of ints to denote how many elements are referencing this vertex
    imstkNew<DataArray<int>> referenceCountPtr(tissueMesh->getNumVertices());

    LOG(DEBUG)<< "getNumVertices: "<<tissueMesh->getNumVertices();
    referenceCountPtr->fill(0);
    tissueMesh->setVertexAttribute("ReferenceCount", referenceCountPtr);
   
    std::shared_ptr<SurfaceMesh>     surfMesh   = tissueMesh->extractSurfaceMesh();

    //Generate colinder cube or plane:


    //Configure behaviour of Model
    imstkNew<PbdModel> pbdModel;
    //pbdModel->setModelGeometry(tissueMesh);
    pbdModel->configure(pbdParams);

    //Configure of VisualModel layer
    imstkNew<RenderMaterial> material;
    material->setBackFaceCulling(false);
    material->setDisplayMode(RenderMaterial::DisplayMode::WireframeSurface);
    material->setShadingModel(RenderMaterial::ShadingModel::PBR);
    //material->setPointSize(10.0);

   /* imstkNew<VisualModel> visualModel;
    visualModel->setGeometry(surfMesh);
    visualModel->setRenderMaterial(material);
    pbdObj->addVisualModel(visualModel);*/

    //Configure physical and visual Object model
    pbdObj->setPhysicsGeometry(tissueMesh);
    pbdObj->setVisualGeometry(tissueMesh);
    pbdObj->getVisualModel(0)->setRenderMaterial(material);
    //pbdObj->setCollidingGeometry(surfMesh);
    //pbdObj->setPhysicsToCollidingMap(std::make_shared<OneToOneMap>(tissueMesh, surfMesh));
    pbdObj->setDynamicalModel(pbdModel);


    //Fijar puntos y extern
    /*if(std::find(pbdParams->m_fixedNodeIds.begin(), pbdParams->m_fixedNodeIds.end(), 
                    i) != pbdParams->m_fixedNodeIds.end()){
           

    }*/

    pbdObj->getPbdBody()->fixedNodeIds =pbdParams->m_fixedNodeIds;
    pbdObj->getPbdBody()->fExtNodeIds =pbdParams->m_fExtNodeIds;



    return pbdObj;
}
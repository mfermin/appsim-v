#include "loadMesh.h"
#include "imstkImageData.h"


using json = nlohmann::json;


const std::string readFileJson(const std::string& fname, const std::shared_ptr<PbdModelConfig> confPBD)  
{  
    //imstkNew<FemModelConfig> config;

    //json root = "{ \"E\": 1E7, \"nu\": 0.4, \"gravity\": 9.8, \"fixPnt\":[91, 92, 93, 94, 95]}"_json;
    //std::ofstream file(fname);
    //file << root;
    //file << std::setw(4) << root << std::endl;
   
    // read from the file
    ifstream in(fname, ios::binary);  

    if( !in.is_open() )    
    {   
    cout << "Error opening configFile\n";   
    return 0;   
    }
    else{
        cout << "Opening configFile\n"; 
        json  jsonread= json::parse(in);
        //LOG(INFO)<<"log Fix";
        for (int i =0; i<jsonread["fixPnt"].size(); i++){
            //LOG(INFO)<< jsonread["fixPnt"][i].get<int>();
            confPBD->m_fixedNodeIds.push_back(jsonread["fixPnt"][i].get<int>());
        }
        //
        LOG(INFO)<<"log Ext";
        for (int i =0; i<jsonread["FExtPnt"].size(); i++){
            //LOG(INFO)<< jsonread["FExtPnt"][i].get<int>();
            confPBD->m_fExtNodeIds.push_back(jsonread["FExtPnt"][i].get<int>());
        }
        confPBD->m_fext = Vec3d(jsonread["fext"][0].get<double>(), jsonread["fext"][1].get<double>(), jsonread["fext"][2].get<double>());
        //f_ctrl = Vec3d(jsonread["fctrl"][0].get<double>(), jsonread["fctrl"][1].get<double>(), jsonread["fctrl"][2].get<double>());
        LOG(INFO)<<"fext: "<< confPBD->m_fext;
        //confFem->m_density = jsonread["density"].get<double>();
        //confFem->m_fixedDOFFilename= std::string
        //std::vector<std::size_t> m_fixedNodeIds;
        //auto fixed_pointed= jsonread["fixPnt"].get<std::vector<int>>(); //std::vector<std::size_t> fixedNodeIds;
        //LOG(INFO)<< fixed_pointed;

        confPBD->m_iterations = jsonread["iteration"].get<double>();;
        confPBD->m_linearDampingCoeff= jsonread["damping"].get<double>();;
        //confPBD->m_uniformMassValue = jsonread["mass"].get<double>(); //dimension of fext vector  //0.06 -> 60g

        confPBD->m_femParams->m_YoungModulus = jsonread["E"].get<double>();;//1000.0;//1000;//350.0; 1E9 //1E8
        confPBD->m_femParams->m_PoissonRatio = jsonread["nu"].get<double>();;//0.3;//0.45;
        //confPBD->m_femParams->m_mu = jsonread["mu"].get<double>(); //paper model: 10
        //confPBD->m_femParams->m_lambda = jsonread["lambda"].get<double>(); //paper : 

        int vertexIdx = jsonread["vertexIdx"].get<int>();

        string materialHyEl= jsonread["HyperElastMat"].get<string>();

        if (materialHyEl == "StVK") confPBD->enableFemConstraint(PbdFemConstraint::MaterialType::StVK);
        else if (materialHyEl == "NeoHookean") confPBD->enableFemConstraint(PbdFemConstraint::MaterialType::NeoHookean);
        else if (materialHyEl == "Corotation") confPBD->enableFemConstraint(PbdFemConstraint::MaterialType::Corotation);
        else if (materialHyEl == "StNeoHookean") confPBD->enableFemConstraint(PbdFemConstraint::MaterialType::StNeoHookean);
        else if (materialHyEl == "StableNeoHookean") confPBD->enableFemConstraint(PbdFemConstraint::MaterialType::StableNeoHookean);
        else if (materialHyEl == "MooneyRivlin") confPBD->enableFemConstraint(PbdFemConstraint::MaterialType::MooneyRivlin);
        else if (materialHyEl == "MooneyRivlin5") confPBD->enableFemConstraint(PbdFemConstraint::MaterialType::MooneyRivlin5);
        

        LOG(INFO)<< "Material: "<< materialHyEl;
        //setParticleMass

       
        //confPBD->enableConstraint(PbdConstraint::Type::FemTet, confPBD->m_FemParams->m_YoungModulus);
        //confPBD->m_gravity    = Vec3d(0, -jsonread["gravity"].get<double>(), 0);
        confPBD->m_gravity = Vec3d(jsonread["gravity"][0].get<double>(), jsonread["gravity"][1].get<double>(), jsonread["gravity"][2].get<double>());
        //confPBD->m_gravity = Vec3d(jsonread["Position"][0].get<double>(), jsonread["Position"][1].get<double>(), jsonread["Position"][2].get<double>());

        //confPBD->m_dt  = 0.00;//0.01;//0.01;//0.008;
        //confPBD->collisionParams->m_proximity = 0.3;
        //confPBD->collisionParams->m_stiffness = 0.01;

        confPBD->m_doPartitioning   = true;
        //confPBD->m_uniformMassValue = 0.1;
        //confPBD->m_gravity    = Vec3d(0.0, -0.2, 0.0);
        confPBD->m_dt         = jsonread["dt"].get<double>();;

        if(DEBUG_ON)LOG(INFO)<<"READ FILE OPEN"<< iMSTK_DATA_ROOT+jsonread["meshFile"].get<string>();
        return iMSTK_DATA_ROOT+jsonread["meshFile"].get<string>();
    }
}

///
/// \brief Spherically project the texture coordinates
///
static void
setSphereTexCoords(std::shared_ptr<SurfaceMesh> surfMesh, const double uvScale)
{
    Vec3d min, max;
    surfMesh->computeBoundingBox(min, max);
    const Vec3d size   = max - min;
    const Vec3d center = (max + min) * 0.5;

    const double radius = (size * 0.5).norm();

    auto                    uvCoordsPtr = std::make_shared<VecDataArray<float, 2>>(surfMesh->getNumVertices());
    VecDataArray<float, 2>& uvCoords    = *uvCoordsPtr.get();
    for (int i = 0; i < surfMesh->getNumVertices(); i++)
    {
        const Vec3d vertex = surfMesh->getVertexPosition(i) - center;

        // Compute phi and theta on the sphere
        const double theta = asin(vertex[0] / radius);
        const double phi   = atan2(vertex[1], vertex[2]);
        uvCoords[i] = Vec2f(phi / (PI * 2.0) + 0.5, theta / (PI * 2.0) + 0.5) * uvScale;
    }
    surfMesh->setVertexTCoords("tcoords", uvCoordsPtr);
}

///
/// \brief Generate a Deformable object simulated with Fem
/// \param name of the object
/// \param position of the object
/////Material ENU default:
//double VolumetricMesh::density_default = 1000;
//double VolumetricMesh::E_default = 1E9;
//double VolumetricMesh::nu_default = 0.45;
/// \adjust:  Material  Stiffness (N/m^2)   Compliance (m^2/N) 
///           Fat        1.0 x 10^3           1.0 x 10^-3
//            Wood       6.0 x 10^9            0.16 x 10^-9
//            Muscle     5.0 x 103             0.2 x 10-3
//            liver      90kPa -> 90000-> 9.0 x10^4  

const size_t nx = 20, ny = 10 / 2, nz = 10 / 2;
std::shared_ptr<PbdObject>
makePBD_Object( const std::string& fname, std::shared_ptr<PbdModel> model, enum Tissue tissue)
{
    Src_path src_path;
    // Create the PBD object
    auto pbdObj = std::make_shared<PbdObject>("ObjectPbd");
    //imstkNew<PbdModelConfig> pbdParams;
    auto pbdParams = std::make_shared<PbdModelConfig>();

    //Reading file & config parameters pbd
    //std::shared_ptr<TetrahedralMesh> tissueMesh = MeshIO::read<TetrahedralMesh>(readFileJson(fname, pbdParams));
    //auto surfMesh = MeshIO::read<SurfaceMesh>("/users/ferminm/devSim/appsim-rel/data/mesh/liver/liver_low.obj");
    //std::shared_ptr<TetrahedralMesh> tissueMesh = MeshIO::read<TetrahedralMesh>("/users/ferminm/devSim/appsim-rel/data/mesh/liver/liver_low.vtk");
    //std::shared_ptr<TetrahedralMesh> tissueMesh = GeometryUtils::createTetrahedralMeshCover(surfMesh, nx, ny, nz);
    
    if(tissue == liver){
     LOG(INFO) << "tissue: liver";   
        src_path.file_name = "/users/ferminm/devSim/appsim-rel/data/mesh/liver/liver_low_tet.vtk";
        model->getConfig()->m_femParams->m_YoungModulus = 90000.0;  // in [Nm2] o 90kPa liver in-vivo  //PORCINO
        model->getConfig()->m_femParams->m_PoissonRatio = 0.49;
        model->getConfig()->enableFemConstraint(PbdFemConstraint::MaterialType::NeoHookean); //StNeoHOo
    }else if (tissue == liver_h){
        src_path.file_name = "/users/ferminm/devSim/appsim-rel/data/mesh/liver/double/liver3.vtk";
        model->getConfig()->m_femParams->m_YoungModulus = 10000.0;  // in [Nm2] o 10kPa liver ex-vivo  //PORCINO
        model->getConfig()->m_femParams->m_PoissonRatio = 0.49;
        model->getConfig()->enableFemConstraint(PbdFemConstraint::MaterialType::NeoHookean); //StNeoHOo
    }else if(tissue == gallblader){
        src_path.file_name = "/users/ferminm/devSim/appsim-rel/data/Organs/Gallblader/gallblader.msh";  
        model->getConfig()->m_femParams->m_YoungModulus = 250000.0;  // in [Nm2] o 250kPa gallblader ex-vivo  
        model->getConfig()->m_femParams->m_PoissonRatio = 0.49;
        model->getConfig()->enableFemConstraint(PbdFemConstraint::MaterialType::NeoHookean); //StNeoHOo
    }else if(tissue == cube){
        src_path.file_name = "/users/ferminm/devSim/appsim-rel/data/mesh/liver/liver_low_tet.vtk";  
    }else if(tissue == tissueBox){
        src_path.file_name = "/users/ferminm/devSim/appsim-rel/data/mesh/liver/liver_low_tet.vtk";  
    }
    //VTKMeshIO::write(tissueMesh, "/users/ferminm/devSim/appsim-rel/data/mesh/liver/convertedMesh.vtk", MeshFileType::VTK);
    //VegaMeshIO::write(tissueMesh, "/users/ferminm/devSim/appsim-rel/data/mesh/liver/convertedMesh.vega", MeshFileType::VEG);
    auto   tissueMesh = MeshIO::read<TetrahedralMesh>(src_path.file_name); 
    CHECK(tissueMesh != nullptr) << "Verify mesh file, incompatible or non-existent.";

    auto vertPtr = std::make_shared<DataArray<int>>(tissueMesh->getNumVertices());
    LOG(INFO)<< "mesh input: "<< src_path.file_name<< " n_verx: " <<tissueMesh->getNumVertices();

    vertPtr->fill(0);
    tissueMesh->setVertexAttribute("ReferenceCount", vertPtr);
   
    //auto surfMesh_high = MeshIO::read<SurfaceMesh>("/users/ferminm/devSim/appsim-rel/data/mesh/liver/double/liver.vtk");
    std::shared_ptr<SurfaceMesh>     surfMesh   = tissueMesh->extractSurfaceMesh();
    
    //std::shared_ptr<SurfaceMesh>     surfMesh   = tissueMesh->extractSurfaceMesh();
    setSphereTexCoords(surfMesh, 1.0);
    //Configure behaviour of Model
    //imstkNew<PbdModel> pbdModel;
   // auto pbdModel  = std::make_shared<PbdModel>();
   // pbdModel->setModelGeometry(tissueMesh);
    //pbdModel->configure(pbdParams);

    //Configure of VisualModel layer
    //imstkNew<RenderMaterial> material;
 /*    auto material = std::make_shared<RenderMaterial>();
     material->setDisplayMode(RenderMaterial::DisplayMode::WireframeSurface);
     material->setBackFaceCulling(false);
     material->setOpacity(0.5);
*/
    //auto material = std::make_shared<RenderMaterial>();
    //material->setDisplayMode(RenderMaterial::DisplayMode::Surface);
    //material->setColor(Color::LightGray);
    /*
        Diffuse = 0, // Also used for albedo
        Normal,
        Roughness,
        Metalness,
        SubsurfaceScattering,
        AmbientOcclusion,
        Cavity,
        Cubemap,
        IrradianceCubeMap,
        RadianceCubeMap,
        ORM,
        BRDF_LUT,
        Emissive,
        Anistropy,
        CoatNormal,
        None
    */

    auto material = std::make_shared<RenderMaterial>();
    //material->setShadingModel(RenderMaterial::ShadingModel::PBR);
    material->setBackFaceCulling(false);
    material->setDisplayMode(RenderMaterial::DisplayMode::Surface);
    material->setShadingModel(RenderMaterial::ShadingModel::PBR);
   /* auto diffuseTex = MeshIO::read<ImageData>("/users/ferminm/devSim/appsim-rel/data/mesh/liver/01_diffuse.png");
    material->addTexture(std::make_shared<TextursurfMeshe>(diffuseTex, Texture::Type::Diffuse));
    auto normalTex = MeshIO::read<ImageData>("/users/ferminm/devSim/appsim-rel/data/mesh/liver/01_normal.png");
    material->addTexture(std::make_shared<Texture>(normalTex, Texture::Type::Normal));
    auto roughTex = MeshIO::read<ImageData>("/users/ferminm/devSim/appsim-rel/data/mesh/liver/01_roughness.png");
    material->addTexture(std::make_shared<Texture>(roughTex, Texture::Type::Roughness));
    auto ambOccTex = MeshIO::read<ImageData>("/users/ferminm/devSim/appsim-rel/data/mesh/liver/01_ambientOcclusion.png");
    material->addTexture(std::make_shared<Texture>(ambOccTex, Texture::Type::AmbientOcclusion));
    //auto ambOccTex = MeshIO::read<ImageData>("/users/ferminm/devSim/appsim-rel/data/mesh/liver/01_height.png");
    //material->addTexture(std::make_shared<Texture>(ambOccTex, Texture::Type::AmbientOcclusion));*/

    auto diffuseTex = MeshIO::read<ImageData>("/users/ferminm/devSim/appsim-rel/data/textures/fleshDiffuse.jpg");
    material->addTexture(std::make_shared<Texture>(diffuseTex, Texture::Type::Diffuse));
    auto normalTex = MeshIO::read<ImageData>("/users/ferminm/devSim/appsim-rel/data/textures/fleshNormal.jpg");
    material->addTexture(std::make_shared<Texture>(normalTex, Texture::Type::Normal));
    auto ormTex = MeshIO::read<ImageData>("/users/ferminm/devSim/appsim-rel/data/textures/fleshORM.jpg");
    material->addTexture(std::make_shared<Texture>(ormTex, Texture::Type::ORM));
    material->setNormalStrength(0.3);   

    auto visualModel = std::make_shared<VisualModel>();
    visualModel->setGeometry(surfMesh);
    visualModel->setRenderMaterial(material);

    // Setup the Object
    pbdObj->addVisualModel(visualModel);
    pbdObj->setPhysicsGeometry(tissueMesh);
    pbdObj->setVisualGeometry(surfMesh);
    pbdObj->setCollidingGeometry(surfMesh);
    auto map = std::make_shared<PointwiseMap>(tissueMesh, surfMesh);
    pbdObj->setPhysicsToCollidingMap(map);
    pbdObj->getVisualModel(0)->setRenderMaterial(material);
    pbdObj->setDynamicalModel(model);
    pbdObj->getPbdBody()->uniformMassValue = 0.6 / tissueMesh->getNumVertices();
    model->getConfig()->setBodyDamping(pbdObj->getPbdBody()->bodyHandle, 0.01);


    std::shared_ptr<VecDataArray<double, 3>> vertices = tissueMesh->getVertexPositions();
    /*for (int i = 0; i < tissueMesh->getNumVertices(); i++)
    {
        const Vec3d& pos = (*vertices)[i];
        if (pos[1] >= 0.001)
        {
            pbdObj->getPbdBody()->fixedNodeIds.push_back(i);
        }
    }*/
    //Puntos Fijos:.
   /* pbdObj->getPbdBody()->fixedNodeIds.push_back(2);
    pbdObj->getPbdBody()->fixedNodeIds.push_back(6);
    pbdObj->getPbdBody()->fixedNodeIds.push_back(8);
    pbdObj->getPbdBody()->fixedNodeIds.push_back(12);
    pbdObj->getPbdBody()->fixedNodeIds.push_back(13);
    pbdObj->getPbdBody()->fixedNodeIds.push_back(19);
    pbdObj->getPbdBody()->fixedNodeIds.push_back(26);
    pbdObj->getPbdBody()->fixedNodeIds.push_back(28);
    pbdObj->getPbdBody()->fixedNodeIds.push_back(29);
    pbdObj->getPbdBody()->fixedNodeIds.push_back(30);
    pbdObj->getPbdBody()->fixedNodeIds.push_back(31);
    pbdObj->getPbdBody()->fixedNodeIds.push_back(32);
    pbdObj->getPbdBody()->fixedNodeIds.push_back(33);
    pbdObj->getPbdBody()->fixedNodeIds.push_back(34);
    pbdObj->getPbdBody()->fixedNodeIds.push_back(35);
    pbdObj->getPbdBody()->fixedNodeIds.push_back(36);
    pbdObj->getPbdBody()->fixedNodeIds.push_back(37);
    pbdObj->getPbdBody()->fixedNodeIds.push_back(54);
    pbdObj->getPbdBody()->fixedNodeIds.push_back(55);*/
    //pbdObj->getPbdBody()->fixedNodeIds.push_back(1);
    //pbdObj->getPbdBody()->fixedNodeIds.push_back(2);
   // pbdObj->getPbdBody()->fixedNodeIds.push_back(3);
   // pbdObj->getPbdBody()->fixedNodeIds.push_back(4);


    pbdObj->initialize();

    return pbdObj;
}

std::shared_ptr<PbdObject>
makePBD_surfMesh( const std::string& fname, std::shared_ptr<PbdModel> model, enum Tissue tissue)
{
    Src_path src_path;
    // Create the PBD object
    auto pbdObj = std::make_shared<PbdObject>("ObjectPbd");
    //imstkNew<PbdModelConfig> pbdParams;
    auto pbdParams = std::make_shared<PbdModelConfig>();

    //Reading file & config parameters pbd
    //std::shared_ptr<TetrahedralMesh> tissueMesh = MeshIO::read<TetrahedralMesh>(readFileJson(fname, pbdParams));
    //auto surfMesh = MeshIO::read<SurfaceMesh>("/users/ferminm/devSim/appsim-rel/data/mesh/liver/liver_low.obj");
    //std::shared_ptr<TetrahedralMesh> tissueMesh = MeshIO::read<TetrahedralMesh>("/users/ferminm/devSim/appsim-rel/data/mesh/liver/liver_low.vtk");
    //std::shared_ptr<TetrahedralMesh> tissueMesh = GeometryUtils::createTetrahedralMeshCover(surfMesh, nx, ny, nz);
    
    if(tissue == liver){
     LOG(INFO) << "tissue: liver";   
        src_path.file_name = "/users/ferminm/devSim/appsim-rel/data/mesh/liver/liver_low_tet.vtk";
    }else if (tissue == liver_h){
        src_path.file_name = "/users/ferminm/devSim/appsim-rel/data/mesh/liver/double/liver3.vtk";

    }else if(tissue == gallblader){
        src_path.file_name = "/users/ferminm/devSim/appsim-rel/data/Organs/Gallblader/gallblader.msh";  

    }else if(tissue == cube){
        src_path.file_name = "/users/ferminm/devSim/appsim-rel/data/mesh/liver/liver_low_tet.vtk";  
    }else if(tissue == tissueBox){
        src_path.file_name = "/users/ferminm/devSim/appsim-rel/data/mesh/liver/liver_low_tet.vtk";  
    }
    //VTKMeshIO::write(tissueMesh, "/users/ferminm/devSim/appsim-rel/data/mesh/liver/convertedMesh.vtk", MeshFileType::VTK);
    //VegaMeshIO::write(tissueMesh, "/users/ferminm/devSim/appsim-rel/data/mesh/liver/convertedMesh.vega", MeshFileType::VEG);
    auto   tissueMesh = MeshIO::read<TetrahedralMesh>(src_path.file_name); 
    CHECK(tissueMesh != nullptr) << "Verify mesh file, incompatible or non-existent.";

    auto vertPtr = std::make_shared<DataArray<int>>(tissueMesh->getNumVertices());
    LOG(INFO)<< "mesh input: "<< src_path.file_name<< " n_verx: " <<tissueMesh->getNumVertices();

    vertPtr->fill(0);
    tissueMesh->setVertexAttribute("ReferenceCount", vertPtr);
   
    //auto surfMesh_high = MeshIO::read<SurfaceMesh>("/users/ferminm/devSim/appsim-rel/data/mesh/liver/double/liver.vtk");
    std::shared_ptr<SurfaceMesh>     surfMesh   = tissueMesh->extractSurfaceMesh();
    
    //std::shared_ptr<SurfaceMesh>     surfMesh   = tissueMesh->extractSurfaceMesh();
    setSphereTexCoords(surfMesh, 1.0);
    //Configure behaviour of Model
    //imstkNew<PbdModel> pbdModel;
   // auto pbdModel  = std::make_shared<PbdModel>();
   // pbdModel->setModelGeometry(tissueMesh);
    //pbdModel->configure(pbdParams);

    //Configure of VisualModel layer
    //imstkNew<RenderMaterial> material;
 /*    auto material = std::make_shared<RenderMaterial>();
     material->setDisplayMode(RenderMaterial::DisplayMode::WireframeSurface);
     material->setBackFaceCulling(false);
     material->setOpacity(0.5);
*/
    //auto material = std::make_shared<RenderMaterial>();
    //material->setDisplayMode(RenderMaterial::DisplayMode::Surface);
    //material->setColor(Color::LightGray);
    /*
        Diffuse = 0, // Also used for albedo
        Normal,
        Roughness,
        Metalness,
        SubsurfaceScattering,
        AmbientOcclusion,
        Cavity,
        Cubemap,
        IrradianceCubeMap,
        RadianceCubeMap,
        ORM,
        BRDF_LUT,
        Emissive,
        Anistropy,
        CoatNormal,
        None
    */

    auto material = std::make_shared<RenderMaterial>();
    //material->setShadingModel(RenderMaterial::ShadingModel::PBR);
    material->setBackFaceCulling(false);
    material->setDisplayMode(RenderMaterial::DisplayMode::Surface);
    material->setShadingModel(RenderMaterial::ShadingModel::PBR);
   /* auto diffuseTex = MeshIO::read<ImageData>("/users/ferminm/devSim/appsim-rel/data/mesh/liver/01_diffuse.png");
    material->addTexture(std::make_shared<TextursurfMeshe>(diffuseTex, Texture::Type::Diffuse));
    auto normalTex = MeshIO::read<ImageData>("/users/ferminm/devSim/appsim-rel/data/mesh/liver/01_normal.png");
    material->addTexture(std::make_shared<Texture>(normalTex, Texture::Type::Normal));
    auto roughTex = MeshIO::read<ImageData>("/users/ferminm/devSim/appsim-rel/data/mesh/liver/01_roughness.png");
    material->addTexture(std::make_shared<Texture>(roughTex, Texture::Type::Roughness));
    auto ambOccTex = MeshIO::read<ImageData>("/users/ferminm/devSim/appsim-rel/data/mesh/liver/01_ambientOcclusion.png");
    material->addTexture(std::make_shared<Texture>(ambOccTex, Texture::Type::AmbientOcclusion));
    //auto ambOccTex = MeshIO::read<ImageData>("/users/ferminm/devSim/appsim-rel/data/mesh/liver/01_height.png");
    //material->addTexture(std::make_shared<Texture>(ambOccTex, Texture::Type::AmbientOcclusion));*/

    auto diffuseTex = MeshIO::read<ImageData>("/users/ferminm/devSim/appsim-rel/data/textures/fleshDiffuse.jpg");
    material->addTexture(std::make_shared<Texture>(diffuseTex, Texture::Type::Diffuse));
    auto normalTex = MeshIO::read<ImageData>("/users/ferminm/devSim/appsim-rel/data/textures/fleshNormal.jpg");
    material->addTexture(std::make_shared<Texture>(normalTex, Texture::Type::Normal));
    auto ormTex = MeshIO::read<ImageData>("/users/ferminm/devSim/appsim-rel/data/textures/fleshORM.jpg");
    material->addTexture(std::make_shared<Texture>(ormTex, Texture::Type::ORM));
    material->setNormalStrength(0.3);   

    auto visualModel = std::make_shared<VisualModel>();
    visualModel->setGeometry(surfMesh);
    visualModel->setRenderMaterial(material);

    // Setup the Object
    pbdObj->addVisualModel(visualModel);
    pbdObj->setPhysicsGeometry(surfMesh);
    pbdObj->setVisualGeometry(surfMesh);
    pbdObj->setCollidingGeometry(surfMesh);
    //auto map = std::make_shared<PointwiseMap>(tissueMesh, surfMesh);
    //pbdObj->setPhysicsToCollidingMap(map);
    pbdObj->getVisualModel(0)->setRenderMaterial(material);
    pbdObj->setDynamicalModel(model);
    //pbdObj->getPbdBody()->uniformMassValue = 0.6 / tissueMesh->getNumVertices();
    model->getConfig()->setBodyDamping(pbdObj->getPbdBody()->bodyHandle, 0.01);
    

    model->getConfig()->enableConstraint(PbdModelConfig::ConstraintGenType::Distance,
    1e4, pbdObj->getPbdBody()->bodyHandle);
    model->getConfig()->enableConstraint(PbdModelConfig::ConstraintGenType::Dihedral,
    1.0, pbdObj->getPbdBody()->bodyHandle);


    std::shared_ptr<VecDataArray<double, 3>> vertices = tissueMesh->getVertexPositions();
    /*for (int i = 0; i < tissueMesh->getNumVertices(); i++)
    {
        const Vec3d& pos = (*vertices)[i];
        if (pos[1] >= 0.001)
        {
            pbdObj->getPbdBody()->fixedNodeIds.push_back(i);
        }
    }*/
    //Puntos Fijos:.
   // pbdObj->getPbdBody()->fixedNodeIds.push_back(2);

    //pbdObj->initialize();

    return pbdObj;
}

std::shared_ptr<AnalyticalGeometry> geometries[] = {
    std::make_shared<Capsule>(Vec3d::Zero(), 0.003, 0.01),
    std::make_shared<Cylinder>(Vec3d::Zero(), 0.05, 0.01),
    std::make_shared<Sphere>(Vec3d::Zero(), 0.002)
};

std::shared_ptr<PbdObject>
make_toolInteract(std::shared_ptr<PbdModel> model)
{
    auto toolObj = std::make_shared<PbdObject>("Tool");

    auto meshVisualTool = MeshIO::read<SurfaceMesh>("/users/ferminm/devSim/appsim-rel/mesh/tool_push.stl");
    meshVisualTool->rotate(Vec3d(0.0, 0.0, 0.0), 3.14, Geometry::TransformType::ApplyToData);
    meshVisualTool->rotate(Vec3d(1.0, 0.0, 0.0), -1.57, Geometry::TransformType::ApplyToData);
    meshVisualTool->scale(Vec3d(0.01, 0.01, 0.01), Geometry::TransformType::ApplyToData);
    

    // Create the object
    toolObj->setVisualGeometry(geometries[2]);
    toolObj->setPhysicsGeometry(geometries[2]);
    toolObj->setCollidingGeometry(geometries[2]);
    toolObj->setDynamicalModel(model);
    toolObj->getPbdBody()->setRigid(
                Vec3d(0.04, 0.0, 0.0),
                0.5,
                Quatd::Identity(),
                Mat3d::Identity() * 1.0);

    // Setup the material
    auto material = std::make_shared<RenderMaterial>();
    material->setBackFaceCulling(false);
    material->setDisplayMode(RenderMaterial::DisplayMode::Surface);
    material->setShadingModel(RenderMaterial::ShadingModel::PBR);


    toolObj->getVisualModel(0)->getRenderMaterial()->setColor(Color::Orange);
    toolObj->getVisualModel(0)->getRenderMaterial()->setDisplayMode(RenderMaterial::DisplayMode::Surface);
    toolObj->getVisualModel(0)->getRenderMaterial()->setBackFaceCulling(false);
    toolObj->getVisualModel(0)->getRenderMaterial()->setLineWidth(1.0);
    toolObj->getVisualModel(0)->getRenderMaterial()->setOpacity(1.0);

    // Add a component for controlling via another device
    auto controller = toolObj->addComponent<PbdObjectController>();
    controller->setControlledObject(toolObj);
    controller->setTranslationScaling(1.0);
    controller->setLinearKs(1000.0);
    controller->setAngularKs(10000.0);
    controller->setUseCritDamping(true);
    controller->setForceScaling(1.0);  //1 N = 1 kg/(m*s^2) = 0.01 kg/(cm*s^2)
    controller->setSmoothingKernelSize(15);
    controller->setUseForceSmoothening(true);

    // Add extra component to tool for the ghost
    auto controllerGhost = toolObj->addComponent<ObjectControllerGhost>("GhostTool");
    controllerGhost->setUseForceFade(true);
    controllerGhost->setController(controller);

    return toolObj;
}



std::shared_ptr<PbdObject>
make_toolCut(std::shared_ptr<PbdModel> model)
{
    auto plane = std::make_shared<Plane>();

    plane->setWidth(1.0);

    std::shared_ptr<SurfaceMesh> toolGeom = GeometryUtils::toTriangleGrid(Vec3d::Zero(),Vec2d(0.3, 0.3), Vec2i(2, 2));

    //std::shared_ptr<SurfaceMesh> toolGeom = GeometryUtils::toSurfaceMesh(plane);


    auto toolObj = std::make_shared<PbdObject>("Tool");
    toolObj->setVisualGeometry(toolGeom);
    toolObj->setCollidingGeometry(toolGeom);
    toolObj->setPhysicsGeometry(toolGeom);
    toolObj->setDynamicalModel(model);
    toolObj->getVisualModel(0)->getRenderMaterial()->setColor(Color(0.7, 0.7, 0.75));
    toolObj->getVisualModel(0)->getRenderMaterial()->setDisplayMode(RenderMaterial::DisplayMode::WireframeSurface);
    toolObj->getVisualModel(0)->getRenderMaterial()->setBackFaceCulling(false);
    //toolObj->getVisualModel(0)->getRenderMaterial()->setLineWidth(1.0);

    toolObj->getPbdBody()->setRigid(
        Vec3d(0.4, 0.4, -0.4),         // Position
        0.2,                          // Mass
        Quatd::Identity(),            // Orientation
        Mat3d::Identity() * 10000.0); // Inertia

    auto controller = toolObj->addComponent<PbdObjectController>();
    controller->setControlledObject(toolObj);
    controller->setTranslationScaling(1.0);
    controller->setLinearKs(1000.0);
    controller->setLinearKd(50.0);
    controller->setAngularKs(10000000.0);
    controller->setAngularKd(500000.0);
    controller->setForceScaling(0.001);
    controller->setSmoothingKernelSize(1);
    controller->setUseForceSmoothening(true);

    return toolObj;
}


static void
addDummyVertexPointSet(std::shared_ptr<PointSet> pointSet)
{
    // Add a dummy vertex to the vertices
    std::shared_ptr<VecDataArray<double, 3>> verticesPtr = pointSet->getVertexPositions();
    VecDataArray<double, 3>&                 vertices    = *verticesPtr;
    vertices.resize(vertices.size() + 1);
    for (int i = vertices.size() - 1; i >= 1; i--)
    {
        vertices[i] = vertices[i - 1];
    }
    vertices[0] = Vec3d(0.0, 0.0, 0.0);
    pointSet->setInitialVertexPositions(std::make_shared<VecDataArray<double, 3>>(*verticesPtr));
}

static void
addDummyVertex(std::shared_ptr<SurfaceMesh> surfMesh)
{
    addDummyVertexPointSet(surfMesh);

    // Then shift all indices by 1
    std::shared_ptr<VecDataArray<int, 3>> indicesPtr = surfMesh->getCells();
    VecDataArray<int, 3>&                 indices    = *indicesPtr;
    for (int i = 0; i < indices.size(); i++)
    {
        indices[i][0]++;
        indices[i][1]++;
        indices[i][2]++;
    }
}

static void
addDummyVertex(std::shared_ptr<TetrahedralMesh> tetMesh)
{
    addDummyVertexPointSet(tetMesh);

    // Then shift all indices by 1
    std::shared_ptr<VecDataArray<int, 4>> tissueIndicesPtr = tetMesh->getCells();
    VecDataArray<int, 4>&                 tissueIndices    = *tissueIndicesPtr;
    for (int i = 0; i < tissueIndices.size(); i++)
    {
        tissueIndices[i][0]++;
        tissueIndices[i][1]++;
        tissueIndices[i][2]++;
        tissueIndices[i][3]++;
    }
}

///
/// \brief Creates tissue object
/// \param name
/// \param physical dimension of tissue
/// \param dimensions of tetrahedral grid used for tissue
/// \param center of tissue block
///
static std::shared_ptr<PbdObject>
makeTissueObj(const std::string& name,
              const Vec3d& size, const Vec3i& dim, const Vec3d& center,
              std::shared_ptr<PbdModel> model)
{
    // Setup the Geometry
    std::shared_ptr<TetrahedralMesh> tissueMesh = GeometryUtils::toTetGrid(center, size, dim);
    std::shared_ptr<SurfaceMesh>     surfMesh   = tissueMesh->extractSurfaceMesh();

    addDummyVertex(tissueMesh);
    //addDummyVertex(surfMesh);

    // Add a mask of ints to denote how many elements are referencing this vertex
    auto referenceCountPtr = std::make_shared<DataArray<int>>(tissueMesh->getNumVertices());
    referenceCountPtr->fill(0);
    tissueMesh->setVertexAttribute("ReferenceCount", referenceCountPtr);

      // Setup the Object
    auto tissueObj = std::make_shared<PbdObject>(name);
    tissueObj->setPhysicsGeometry(tissueMesh);
    tissueObj->setVisualGeometry(tissueMesh);
    tissueObj->setCollidingGeometry(surfMesh);
    auto map = std::make_shared<PointwiseMap>(tissueMesh, surfMesh);
    tissueObj->setPhysicsToCollidingMap(map);
    

    // Use FEMTet constraints
    model->getConfig()->m_femParams->m_YoungModulus = 500000.0;
    model->getConfig()->m_femParams->m_PoissonRatio = 0.4;
    model->getConfig()->enableFemConstraint(PbdFemConstraint::MaterialType::StVK);

    //model->getConfig()->m_femParams->m_YoungModulus = 108000.0;
    //model->getConfig()->m_femParams->m_PoissonRatio = 0.4;
    //model->getConfig()->enableFemConstraint(PbdFemConstraint::MaterialType::NeoHookean);
    //model->getConfig()->setBodyDamping(tissueObj->getPbdBody()->bodyHandle, 0.01);

    // Setup the material
    auto material = std::make_shared<RenderMaterial>();
    material->setBackFaceCulling(false);
    material->setOpacity(0.5);
    material->setDisplayMode(RenderMaterial::DisplayMode::WireframeSurface);
    material->setShadingModel(RenderMaterial::ShadingModel::PBR);

    tissueObj->getVisualModel(0)->setRenderMaterial(material);
    tissueObj->setDynamicalModel(model);
    tissueObj->getPbdBody()->uniformMassValue = 0.1;

  
    // Fix the borders
    for (int z = 0; z < dim[2]; z++)
    {
        for (int y = 0; y < dim[1]; y++)
        {
            for (int x = 0; x < dim[0]; x++)
            {
                if (x == 0 || /*z == 0 ||*/ x == dim[0] - 1 /*|| z == dim[2] - 1*/)
                {
                    tissueObj->getPbdBody()->fixedNodeIds.push_back(x + dim[0] * (y + dim[1] * z) + 1); // +1 for dummy vertex
                }
            }
        }
    }
    tissueObj->getPbdBody()->fixedNodeIds.push_back(0); // Fix dummy vertex

    return tissueObj;
}

static std::shared_ptr<PbdObject>
makePbdObjCube(
    const std::string&        name,
    std::shared_ptr<PbdModel> model,
    const Vec3d&              size,
    const Vec3i&              dim,
    const Vec3d&              center)
{
    auto prismObj = std::make_shared<PbdObject>(name);

    // Setup the Geometry
    std::shared_ptr<TetrahedralMesh> tissueMesh = GeometryUtils::toTetGrid(center, size, dim);
     //const Vec3d center     = tissueMesh->getCenter();
    tissueMesh->translate(-center, Geometry::TransformType::ApplyToData);
    tissueMesh->scale(1.0, Geometry::TransformType::ApplyToData);
    tissueMesh->rotate(Vec3d(0.0, 0.0, 1.0), 30.0 / 180.0 * 3.14, Geometry::TransformType::ApplyToData);

    const Vec3d shift = { -0.0, 0.0, 0.0 };
    tissueMesh->translate(shift, Geometry::TransformType::ApplyToData);

    auto surfMesh = tissueMesh->extractSurfaceMesh();

    // Setup the material
    auto material = std::make_shared<RenderMaterial>();
    material->setDisplayMode(RenderMaterial::DisplayMode::WireframeSurface);
    material->setBackFaceCulling(false);
    material->setOpacity(0.5);

    // Add a visual model to render the tet mesh
    auto visualModel = std::make_shared<VisualModel>();
    visualModel->setGeometry(surfMesh);
    visualModel->setRenderMaterial(material);

    // Setup the Object
    auto tissueObj = std::make_shared<PbdObject>(name);
    tissueObj->addVisualModel(visualModel);
    //tissueObj->addVisualModel(labelModel);
    tissueObj->setPhysicsGeometry(tissueMesh);
    tissueObj->setCollidingGeometry(surfMesh);
    tissueObj->setDynamicalModel(model);

    tissueObj->setPhysicsToCollidingMap(std::make_shared<PointwiseMap>(tissueMesh, surfMesh));

    // Gallblader is about 60g
    tissueObj->getPbdBody()->uniformMassValue = 0.6 / tissueMesh->getNumVertices();

    model->getConfig()->m_femParams->m_YoungModulus = 108000.0;
    model->getConfig()->m_femParams->m_PoissonRatio = 0.4;
    model->getConfig()->enableFemConstraint(PbdFemConstraint::MaterialType::NeoHookean);
    model->getConfig()->setBodyDamping(tissueObj->getPbdBody()->bodyHandle, 0.01);

    // tissueObj->getPbdBody()->fixedNodeIds = { 57, 131, 132 }; // { 72, , 131, 132 };

    // Fix the borders
    std::shared_ptr<VecDataArray<double, 3>> vertices = tissueMesh->getVertexPositions();
    for (int i = 0; i < tissueMesh->getNumVertices(); i++)
    {
        const Vec3d& pos = (*vertices)[i];
        if (pos[1] >= 0.016)
        {
            tissueObj->getPbdBody()->fixedNodeIds.push_back(i);
        }
    }

    LOG(INFO) << "Per particle mass: " << tissueObj->getPbdBody()->uniformMassValue;

    return tissueObj;
}

std::shared_ptr<SurfaceMesh>
createCollidingSurfaceMesh()
{
    imstkNew<VecDataArray<double, 3>> verticesPtr;
    VecDataArray<double, 3>&          vertices = *verticesPtr.get();
    int                               nSides   = 2;
    double                            width    = 5.0;
    double                            height   = 5.0;
    int                               nRows    = 2;
    int                               nCols    = 2;
    vertices.resize(nRows * nCols * nSides);
    const double dy = width / static_cast<double>(nCols - 1);
    const double dx = height / static_cast<double>(nRows - 1);
    for (int i = 0; i < nRows; ++i)
    {
        for (int j = 0; j < nCols; j++)
        {
            const double y = static_cast<double>(dy * j);
            const double x = static_cast<double>(dx * i);
            vertices[i * nCols + j] = Vec3d(x - 20, -10.0, y - 20);
        }
    }

    // c. Add connectivity data
    std::shared_ptr<VecDataArray<int, 3>> trianglesPtr = std::make_shared<VecDataArray<int, 3>>();
    VecDataArray<int, 3>&                 triangles    = *trianglesPtr;
    for (int i = 0; i < nRows - 1; ++i)
    {
        for (int j = 0; j < nCols - 1; j++)
        {
            triangles.push_back(Vec3i(i * nCols + j, i * nCols + j + 1, (i + 1) * nCols + j));
            triangles.push_back(Vec3i((i + 1) * nCols + j + 1, (i + 1) * nCols + j, i * nCols + j + 1));
        }
    }

    int nPointPerSide = nRows * nCols;
    //sidewalls 1 and 2 of box
    width  = 5.0;
    height = 5.0;
    nRows  = 2;
    nCols  = 2;
    const double dz  = width / static_cast<double>(nCols - 1);
    const double dx1 = height / static_cast<double>(nRows - 1);
    for (int i = 0; i < nRows; ++i)
    {
        for (int j = 0; j < nCols; j++)
        {
            const double z = static_cast<double>(dz * j);
            const double x = static_cast<double>(dx1 * i);
            vertices[(nPointPerSide) + i * nCols + j]     = Vec3d(x - 20, z - 10.0, 20);
            vertices[(nPointPerSide * 2) + i * nCols + j] = Vec3d(x - 20, z - 10.0, -20);
        }
    }

    // c. Add connectivity data
    for (int i = 0; i < nRows - 1; ++i)
    {
        for (int j = 0; j < nCols - 1; j++)
        {
            triangles.push_back(Vec3i((nPointPerSide) + i * nCols + j, (nPointPerSide) + i * nCols + j + 1, (nPointPerSide) + (i + 1) * nCols + j));
            triangles.push_back(Vec3i((nPointPerSide) + (i + 1) * nCols + j + 1, (nPointPerSide) + (i + 1) * nCols + j, (nPointPerSide) + i * nCols + j + 1));

            triangles.push_back(Vec3i((nPointPerSide * 2) + i * nCols + j + 1, (nPointPerSide * 2) + i * nCols + j, (nPointPerSide * 2) + (i + 1) * nCols + j));
            triangles.push_back(Vec3i((nPointPerSide * 2) + (i + 1) * nCols + j, (nPointPerSide * 2) + (i + 1) * nCols + j + 1, (nPointPerSide * 2) + i * nCols + j + 1));
        }
    }

    //sidewalls 3 and 4 of box
    width  = 5.0;
    height = 5.0;
    nRows  = 2;
    nCols  = 2;
    const double dz1 = width / static_cast<double>(nCols - 1);
    const double dy1 = height / static_cast<double>(nRows - 1);
    for (int i = 0; i < nRows; ++i)
    {
        for (int j = 0; j < nCols; j++)
        {
            const double z = static_cast<double>(dz1 * j);
            const double y = static_cast<double>(dy1 * i);
            vertices[(nPointPerSide * 3) + i * nCols + j] = Vec3d(20, z - 10.0, y - 20);
            vertices[(nPointPerSide * 4) + i * nCols + j] = Vec3d(-20, z - 10.0, y - 20);
        }
    }

    // c. Add connectivity data
    for (int i = 0; i < nRows - 1; ++i)
    {
        for (int j = 0; j < nCols - 1; j++)
        {
            triangles.push_back(Vec3i((nPointPerSide * 3) + i * nCols + j + 1, (nPointPerSide * 3) + i * nCols + j, (nPointPerSide * 3) + (i + 1) * nCols + j));
            triangles.push_back(Vec3i((nPointPerSide * 3) + (i + 1) * nCols + j, (nPointPerSide * 3) + (i + 1) * nCols + j + 1, (nPointPerSide * 3) + i * nCols + j + 1));

            triangles.push_back(Vec3i((nPointPerSide * 4) + i * nCols + j, (nPointPerSide * 4) + i * nCols + j + 1, (nPointPerSide * 4) + (i + 1) * nCols + j));
            triangles.push_back(Vec3i((nPointPerSide * 4) + (i + 1) * nCols + j + 1, (nPointPerSide * 4) + (i + 1) * nCols + j, (nPointPerSide * 4) + i * nCols + j + 1));
        }
    }

    imstkNew<SurfaceMesh> floorMeshColliding;
    floorMeshColliding->initialize(verticesPtr, trianglesPtr);
    return floorMeshColliding;
}

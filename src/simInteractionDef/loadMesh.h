/*
** This code based in library: iMSTK, nlohmann_json, Vrpn
** The iMSTK is distributed under the Apache License, Version 2.0.
** The Nlohmann_json library is licensed under the MIT License.
** The VRPN library is licensed under the Boost Software License 1.0
** for more details see Readme.md.
*/
#include "imstkCamera.h"
#include "imstkCapsule.h"
#include "imstkControllerForceText.h"
#include "imstkDirectionalLight.h"
#include "imstkGeometryUtilities.h"
#include "imstkKeyboardDeviceClient.h"
#include "imstkKeyboardSceneControl.h"
#include "imstkMouseDeviceClient.h"
#include "imstkMouseSceneControl.h"
#include "imstkObjectControllerGhost.h"
#include "imstkPbdModel.h"
#include "imstkPbdModelConfig.h"
#include "imstkPbdObject.h"
#include "imstkPbdObjectCollision.h"
#include "imstkPbdObjectController.h"
#include "imstkPbdRigidObjectGrasping.h"
#include "imstkPointwiseMap.h"
#include "imstkRenderMaterial.h"
#include "imstkScene.h"
#include "imstkSceneManager.h"
#include "imstkSimulationManager.h"
#include "imstkSimulationUtils.h"
#include "imstkVisualModel.h"
#include "imstkVTKViewer.h"
#include "imstkMeshIO.h"
#include "imstkVTKMeshIO.h"
#include "imstkPlane.h"
#include "imstkNew.h"


//add pinza:
#include "imstkPbdRigidObjectGrasping.h"
#include "imstkObjectControllerGhost.h"
#include "imstkCapsule.h"
#include "imstkCapsule.h"
#include "imstkCylinder.h"
#include "imstkSphere.h"
#include "imstkControllerForceText.h"
#include "nlohmann/json.hpp"
#include <thread>

#include <vector>


#include <memory>

using namespace imstk;
using namespace std;
//using namespace vega;

enum Tissue
{
    liver = 0,
    liver_h,
    gallblader,
    cube,
    tissueBox

};
struct Src_path
{
    std::string file_name;
    std::vector<std::size_t> fix_nodes;
};

std::shared_ptr<PbdObject> makePBD_Object(const std::string& fname, std::shared_ptr<PbdModel> model, enum Tissue tissue);
std::shared_ptr<PbdObject> makeTissueObjCube(const std::string& name, const Vec3d& size, const Vec3i& dim, const Vec3d& center, std::shared_ptr<PbdModel> model);
static std::shared_ptr<PbdObject> makePbdObjCube(const std::string&        name, std::shared_ptr<PbdModel> model,  const Vec3d&  size, const Vec3i&  dim,  const Vec3d&  center);
static std::shared_ptr<PbdObject> makeTissueObj(const std::string& name,   const Vec3d& size, const Vec3i& dim, const Vec3d& center,  std::shared_ptr<PbdModel> model);
static void addDummyVertex(std::shared_ptr<TetrahedralMesh> tetMesh);
static void addDummyVertex(std::shared_ptr<SurfaceMesh> surfMesh);
static void addDummyVertexPointSet(std::shared_ptr<PointSet> pointSet);

const std::string readFileJson(const std::string& fname, const std::shared_ptr<PbdModelConfig> confPBD);
std::shared_ptr<PbdObject> make_toolInteract(std::shared_ptr<PbdModel> model);
std::shared_ptr<PbdObject> make_toolCut(std::shared_ptr<PbdModel> model);
std::shared_ptr<SurfaceMesh> createCollidingSurfaceMesh();


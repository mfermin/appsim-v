/*
** This code based in library: iMSTK, nlohmann_json, Vrpn
** The iMSTK is distributed under the Apache License, Version 2.0.
** The Nlohmann_json library is licensed under the MIT License.
** The VRPN library is licensed under the Boost Software License 1.0
** for more details see Readme.md.
*/
#include "loadMesh.h"
#include "imstkCamera.h"
#include "imstkCameraController.h"
#include "imstkControllerForceText.h"
#include "imstkDirectionalLight.h"
#include "imstkGeometryUtilities.h" 
#include "imstkKeyboardDeviceClient.h"
#include "imstkKeyboardSceneControl.h"
#include "imstkMouseDeviceClient.h"
#include "imstkMouseSceneControl.h"
#include "imstkObjectControllerGhost.h"
#include "imstkPbdModel.h"
//#include "../libPatch/imstkPbdModel_mod.h"
#include "imstkPbdModelConfig.h"
#include "imstkPbdObject.h"
#include "imstkPbdObjectCollision.h"
#include "imstkPbdObjectController.h"
#include "imstkPbdRigidObjectGrasping.h"
#include "imstkPointwiseMap.h"
#include "imstkRenderMaterial.h"
#include "imstkScene.h"
#include "imstkSceneManager.h"
#include "imstkSimulationManager.h"
#include "imstkSimulationUtils.h"
#include "imstkVisualModel.h"
#include "imstkVTKViewer.h"
#include "imstkMeshIO.h"
#include "imstkVegaMeshIO.h"
#include "imstkSpotLight.h"

//intro controller: 
#include "imstkLaparoscopicToolController.h"

//addings
#include "imstkTaskGraphVizWriter.h"


#ifndef iMSTK_USE_HAPDEV
#include "imstkDummyClient.h"
#include "imstkMouseDeviceClient.h"
#endif

//add pinza:
#include "imstkPbdRigidObjectGrasping.h"
#include "imstkObjectControllerGhost.h"
#include "imstkCapsule.h"
#include "imstkCylinder.h"
#include "imstkSphere.h"
#include "imstkControllerForceText.h"
#include "CutHelp.h"

//add cutting tool: 
#include "imstkPbdObjectCutting.h"
#include "imstkPbdObjectCellRemoval.h"

//logging
//#include "imstkDataLogger.h"
#include<iostream>


using namespace imstk;
//using namespace libPatch;

//#define OPTION_GRASP 1

//#define OPTION_CUT 1
bool flag_rec =false; 
bool flag_rec_vtkMesh =false;
bool flag_rec_vtkMesh_2 = false;
double dt_temp = 0.0;


///
const std::string& configFileJson  = "/users/ferminm/devSim/appsim-rel/data/mesh/simulador_corte.json";

int
main()
{
    //logging  start writer
    Logger::startLogger();
    StopWatch timer;
    int  currMatId  = 0;
    int  prevMatId  = -1;
    


     auto scene = std::make_shared<Scene>("PbdHapticGrasping");
    //scene->getActiveCamera()->setPosition(0.00610397, 0.131126, 0.281497);
    //scene->getActiveCamera()->setFocalPoint(0.0, 0.0, 0.0);
    //scene->getActiveCamera()->setViewUp(0.00251247, 0.90946, -0.415783);

     std::shared_ptr<PointSet> m_mesh_coompared;

    // cutinng view: 
    scene->getActiveCamera()->setPosition(Vec3d(0.0, 0.0, 1));

    // Lighting
    auto light = std::make_shared<SpotLight>();
    light->setFocalPoint(Vec3d(0.0, 0.0, 0.0));
    light->setPosition(Vec3d(1., .0, .4));
    light->setIntensity(2.0);
    light->setSpotAngle(20.0);
    //light->setAttenuationValues(0.0, 0.0, 1.0); // Constant
    //light->setAttenuationValues(0.0, 0.5, 0.0); // Linear falloff
    light->setAttenuationValues(10.0, 0.0, 0.0); // Quadratic
    scene->addLight("light0", light);

    auto light2= std::make_shared<SpotLight>();
    light2->setFocalPoint(Vec3d(0.0, 0.0, 0.0));
    light2->setPosition(Vec3d(0., .0, .8));
    light2->setIntensity(10.0);
    light2->setSpotAngle(50.0);
    //light->setAttenuationValues(0.0, 0.0, 1.0); // Constant
    //light->setAttenuationValues(0.0, 0.5, 0.0); // Linear falloff
    //light->setAttenuationValues(10.0, 0.0, 0.0); // Quadratic
    scene->addLight("light1", light2);


#if OPTION_GRASP

    auto geomShaft = std::make_shared<Capsule>();
    geomShaft->setLength(1.0);
    geomShaft->setRadius(0.005);
    geomShaft->setOrientation(Quatd(Rotd(PI_2, Vec3d(1.0, 0.0, 0.0))));
    geomShaft->setTranslation(Vec3d(0.0, 0.0, 0.5));
    auto objShaft = std::make_shared<CollidingObject>("ShaftObject");
    objShaft->setVisualGeometry(MeshIO::read<SurfaceMesh>("/users/ferminm/devSim/appsim-rel/data/mesh/tools/LapTool/pivot.obj"));
    objShaft->setCollidingGeometry(geomShaft);
    scene->addSceneObject(objShaft);

    auto geomUpperJaw = std::make_shared<Capsule>();
    geomUpperJaw->setLength(0.05);
    geomUpperJaw->setTranslation(Vec3d(0.0, 0.0013, -0.016));
    geomUpperJaw->setRadius(0.004);
    geomUpperJaw->setOrientation(Quatd(Rotd(PI_2, Vec3d(1.0, 0.0, 0.0))));
    auto objUpperJaw = std::make_shared<CollidingObject>("UpperJawObject");
    objUpperJaw->setVisualGeometry(MeshIO::read<SurfaceMesh>("/users/ferminm/devSim/appsim-rel/data/mesh/tools/LapTool/upper.obj"));
    objUpperJaw->setCollidingGeometry(geomUpperJaw);
    scene->addSceneObject(objUpperJaw);

    auto geomLowerJaw = std::make_shared<Capsule>();
    geomLowerJaw->setLength(0.05);
    geomLowerJaw->setTranslation(Vec3d(0.0, -0.0013, -0.016));
    geomLowerJaw->setRadius(0.004);
    geomLowerJaw->setOrientation(Quatd(Rotd(PI_2, Vec3d(1.0, 0.0, 0.0))));
    auto objLowerJaw = std::make_shared<CollidingObject>("LowerJawObject");
    objLowerJaw->setVisualGeometry(MeshIO::read<SurfaceMesh>("/users/ferminm/devSim/appsim-rel/data/mesh/tools/LapTool/lower.obj"));
    objLowerJaw->setCollidingGeometry(geomLowerJaw);
    scene->addSceneObject(objLowerJaw);

    auto pickGeom = std::make_shared<Capsule>();
    pickGeom->setLength(0.05);
    pickGeom->setTranslation(Vec3d(0.0, 0.0, -0.016));
    pickGeom->setRadius(0.006);
    pickGeom->setOrientation(Quatd(Rotd(PI_2, Vec3d(1.0, 0.0, 0.0))));

#endif


    auto pbdModel  = std::make_shared<PbdModel>();
    std::shared_ptr<PbdModelConfig> pbdParams = pbdModel->getConfig();
    pbdParams->m_gravity    = Vec3d(0.0, -2.0, 0.0);
    pbdParams->m_dt         = 0.005;
    pbdParams->m_iterations = 8;
    pbdParams->m_linearDampingCoeff = 0.03;
    // Setup a tissue
    //std::shared_ptr<PbdObject> tissueObj = makeTissueObj("MeshTissue",Vec3d(0.5, 0.1, 0.5), Vec3i(10, 5, 5), Vec3d(0.0, 0.0, 0.0),pbdModel);
    //std::shared_ptr<PbdObject> tissueObj = makeTissueObj("Tissue", Vec3d(5.0, 5.0, 5.0), Vec3i(5,5, 5), Vec3d(0.0, 0.0, 0.0), pbdModel);

    std::shared_ptr<PbdObject> tissueObj = makePBD_Object(configFileJson, pbdModel, liver_h);
    //std::shared_ptr<PbdObject> tissueObj = makeTissueObjCube("Tissue",  Vec3d(10.0, 3.0, 10.0), Vec3i(10, 3, 10), Vec3d(0.0, -1.0, 0.0),pbdModel);

    //std::shared_ptr<PbdObject> tissueObj= makeGallBladder("GallBlader Tissue", pbdModel);

    //std::shared_ptr<PbdObject> tissueObj= makeLiver("Liver Tissue", pbdModel);


   //std::shared_ptr<PbdObject> tissueObj = makeTissueObj ("Tissue",
    //    Vec3d(4.0, 4.0, 4.0), Vec3i(5, 5, 5), Vec3d(0.0, 0.0, 0.0), 
     //   pbdModel);


    //std::shared_ptr<PbdObject> tissueObj = makePbdObjCube("Tissue",
      //          pbdModel,
        //        Vec3d(4.0, 4.0, 4.0),  // Dimensions
          //      Vec3i(5, 5, 5),        // Divisions
            //    Vec3d(0.0, -4.0, 0.0)); // Center

    scene->addSceneObject(tissueObj);

#ifdef OPTION_CUT
    auto cellRemoval = std::make_shared<PbdObjectCellRemoval>(tissueObj);
    scene->addInteraction(cellRemoval);

#endif
    std::shared_ptr<PbdObject> toolObj = make_toolInteract(pbdModel);
    std::shared_ptr<PbdObject> toolObjCut = make_toolCut(pbdModel);
    scene->addSceneObject(toolObjCut);
    scene->addSceneObject(toolObj);



    auto pbdToolCollision = std::make_shared<PbdObjectCollision>(tissueObj, toolObj);
    pbdToolCollision->setRigidBodyCompliance(0.0001); // Helps with smoothness
    pbdToolCollision->setUseCorrectVelocity(true);
    scene->addInteraction(pbdToolCollision);

    // Create new picking with constraints
    auto toolPicking = std::make_shared<PbdObjectGrasping>(tissueObj, toolObj);
    toolPicking->setStiffness(0.3);
    scene->addInteraction(toolPicking);

#ifdef OPTION_CUT
    auto toolPickingCut = std::make_shared<PbdObjectGrasping>(tissueObj, toolObjCut);
    toolPickingCut->setStiffness(0.3);
    scene->addInteraction(toolPickingCut);
#endif
    //simulation running
    {
        //adjust rendering
        auto viewer = std::make_shared<VTKViewer>();
        viewer->setWindowTitle("AppSim");
        viewer->setActiveScene(scene);
        viewer->setVtkLoggerMode(VTKViewer::VTKLoggerMode::MUTE);
        //viewer->setDebugAxesLength(0.01, 0.01, 0.01);
        //viewer->setBackgroundColors(Color(210.0 / 255.0, 212.0 / 255.0, 215.0 / 255.0));
        
        //viewer->setBackgroundColors(Color(202.0 / 255.0, 212.0 / 255.0, 157.0 / 255.0));
        //viewer->setBackgroundColors(Color::Black);

        //Scene
        auto sceneManager = std::make_shared<SceneManager>();
        sceneManager->setActiveScene(scene);
        sceneManager->pause();         // Start simulation paused

        auto driver = std::make_shared<SimulationManager>();
        viewer->setBackgroundColors(Color(0.0, 0.0, 0.0), Color(0.800, 0.800, 0.800), true);
        driver->addModule(viewer);
        driver->addModule(sceneManager);
        driver->setDesiredDt(0.002);

        //imstkNew<DataLogger> loggerDisplPBD("saveAllDisplPBD");

        //Mouse client or joystick
        auto deviceClient = std::make_shared<DummyClient>();

#ifdef OPTION_CUT
        auto controller = toolObjCut->getComponent<PbdObjectController>();
#else
        auto controller = toolObj->getComponent<PbdObjectController>();
#endif
        controller->setPosition(Vec3d(0.0, 0.0, 0.0));
        controller->setDevice(deviceClient);

        // controller viewer
        //auto camController = std::make_shared<CameraController>();
        //camController->setCamera(scene->getActiveCamera());
        //camController->setDevice(deviceClient);
        //camController->setOffsetUsingCurrentCameraPose();
        //scene->addControl(camController);

        auto planeGeom = std::make_shared<Plane>();
        planeGeom->setWidth(5.0);
        planeGeom->setPosition(0.0, -.05, 0.0);

        auto planeObj= std::make_shared<CollidingObject>(); 
        planeObj->setVisualGeometry(planeGeom);
        planeObj->setCollidingGeometry(planeGeom);
        planeObj->getVisualModel(0)->getRenderMaterial()->setColor(Color::LightGray);
        scene->addSceneObject(planeObj);


       /* auto floorObj=std::make_shared<CollidingObject>() ;
        std::shared_ptr<SurfaceMesh> floorGeom = createCollidingSurfaceMesh();
        floorObj->setVisualGeometry(floorGeom);
        floorObj->setCollidingGeometry(floorGeom);
        scene->addSceneObject(floorObj);*/

        // Collisions
        auto collisionInteraction = std::make_shared<PbdObjectCollision>(tissueObj, planeObj);
        collisionInteraction->setFriction(100.0);
        collisionInteraction->setDeformableStiffnessA(0.5);
        scene->addInteraction(collisionInteraction);

#ifdef OPTION_GRASP

        auto controllerPara = std::make_shared<LaparoscopicToolController>();
        controllerPara->setParts(objShaft, objUpperJaw, objLowerJaw, pickGeom);
        controllerPara->setDevice(deviceClient);
        controllerPara->setJawAngleChange(1.0);
        scene->addControl(controllerPara);

        auto upperJawCollision = std::make_shared<PbdObjectCollision>(tissueObj, objUpperJaw);
        auto lowerJawCollision = std::make_shared<PbdObjectCollision>(tissueObj, objLowerJaw);
        scene->addInteraction(upperJawCollision);
        scene->addInteraction(lowerJawCollision);

        auto jawPicking = std::make_shared<PbdObjectGrasping>(tissueObj);
        jawPicking->setGeometryToPick(tissueObj->getVisualGeometry(),
            std::dynamic_pointer_cast<PointwiseMap>(tissueObj->getPhysicsToCollidingMap()));
        scene->addInteraction(jawPicking);
#endif
        std::shared_ptr<Entity> mouseAndKeyControls =
            SimulationUtils::createDefaultSceneControl(driver);
        scene->addSceneObject(mouseAndKeyControls);
                connect<Event>(sceneManager, &SceneManager::postUpdate,
            [&](Event*)
            {
                const Vec2d mousePos = viewer->getMouseDevice()->getPos();
                const Vec3d worldPos = Vec3d(mousePos[0] - 0.5, mousePos[1] - 0.5, 0.0) * 0.4;

                deviceClient->setPosition(worldPos);

                if (flag_rec){
                    if(dt_temp != sceneManager->getDt()){

                    std::string  _log = std::to_string(dt_temp);
                    LOG(INFO)<< "dt_time: "<< _log;
                    //std::string mesh_name = "./mesh_log/mesh-"+std::to_string(tissueObj->getPbdModel()->getConfig()->m_varPos)+_log+".vtk";
                    //VTKMeshIO::write(m_geom, mesh_name, MeshFileType::VTK);
                    
                    //LOGGER POSITIONS IN TXT //loggerPosition->log("# X Y Z f xPBD\n", true);
                    auto nVertx= m_mesh_coompared->getNumVertices();
                    LOG(INFO)<< "log # X Y Z dX dY dZ xPBD n: "<< nVertx<<"\n";
                    ofstream fileWrite;
                    fileWrite.open("./mesh_log/"+_log+".txt");
                    for (int i=0; i<nVertx;i++){
                        
                        auto &ip= m_mesh_coompared->getInitialVertexPosition(i); //->getVertexPosition(i);
                        auto m_geom = std::static_pointer_cast<TetrahedralMesh>(tissueObj->getPhysicsGeometry());
                        auto m_mesh = std::dynamic_pointer_cast<PointSet>(m_geom);
                        auto &p= m_mesh->getVertexPosition(i);
                        auto dp= p-ip;
                        float disp_p= std::sqrt(std::pow(dp[0],2)+std::pow(dp[1],2)+std::pow(dp[2],2));
                        
                        //std::string message = "# "+ std::to_string(i)+"| (" + std::to_string(p[0])+", " + std::to_string(p[1])+", " + std::to_string(p[2]) + ")\n";
                        std::string message = std::to_string(ip[0])
                            + ", " + std::to_string(ip[1])
                            + ", " + std::to_string(ip[2])
                            + ", " + std::to_string(dp[0]) 
                            + ", " + std::to_string(dp[1])
                            + ", " + std::to_string(dp[2])
                            + "\n";
                        fileWrite<< message;
                        
                        //loggerDisplPBD->log(message, false);
                    }fileWrite.close();
                    //loggerDisplPBD->shutdown();
                    //

                    }
                    dt_temp = sceneManager->getDt();
                }else if(flag_rec_vtkMesh){
                    prevMatId = currMatId;
                    currMatId= static_cast<int>(timer.getTimeElapsed() / 1000.0);
                    if(currMatId != prevMatId){

                        std::string  _log = std::to_string(currMatId);
                        //LOG(INFO)<< "dt_time: "<< _log;
                        auto m_geom = std::static_pointer_cast<TetrahedralMesh>(tissueObj->getPhysicsGeometry());

                        std::string mesh_name = "./mesh_log/mesh-"+std::to_string(tissueObj->getPbdModel()->getConfig()->m_varPos)+_log+".vtk";
                        VTKMeshIO::write(m_geom, mesh_name, MeshFileType::VTK);
                    }
                    dt_temp = sceneManager->getDt();
                }else if(flag_rec_vtkMesh_2){
                    if(dt_temp != sceneManager->getDt()){
                        std::string  _log = std::to_string(dt_temp);
                        LOG(INFO)<< "dt_time: "<< _log;
                        auto m_geom = std::static_pointer_cast<TetrahedralMesh>(tissueObj->getPhysicsGeometry());
                        std::shared_ptr<SurfaceMesh>     surfaceMesh   = m_geom->extractSurfaceMesh();

                        std::string mesh_name = "./mesh_log/mesh-"+std::to_string(tissueObj->getPbdModel()->getConfig()->m_varPos)+_log+".vtk";
                        VTKMeshIO::write(surfaceMesh, mesh_name, MeshFileType::VTK);
                    }
                    dt_temp = sceneManager->getDt();
                }


             });
        connect<MouseEvent>(viewer->getMouseDevice(), &MouseDeviceClient::mouseScroll,
            [&](MouseEvent* e)
            {

                const Quatd delta = Quatd(Rotd(e->m_scrollDx * 0.1, Vec3d(0.0, 0.0, 1.0)));
                deviceClient->setOrientation(deviceClient->getOrientation() * delta);
            });

        // effect after click w mouse
        connect<Event>(viewer->getMouseDevice(), &MouseDeviceClient::mouseButtonPress,
            [&](Event*)
            {
                //toolPicking->beginVertexGrasp(std::dynamic_pointer_cast<Sphere>(toolObj->getCollidingGeometry()));
                //pbdToolCollision->setEnabled(false);
               // const Vec3d pos = camController->getPosition();
                //const Quatd orientation = camController->getOrientation();

                //light->setPosition(pos);
                //light->setFocalPoint(pos - orientation.toRotationMatrix().col(2));
            });
        ///

        //grasper w keyboard controller
        connect<KeyEvent>(viewer->getKeyboardDevice(), &KeyboardDeviceClient::keyPress,
            [&](KeyEvent* e)
            {
                if (e->m_key == 'g')
                {
                #ifdef OPTION_GRASP
                    LOG(INFO) << "Jaw Closed!";

                    upperJawCollision->setEnabled(false);
                    lowerJawCollision->setEnabled(false);
                    //jawPicking->beginCellGrasp(pickGeom, "SurfaceMeshToCapsuleCD");
                    jawPicking->beginRayPointGrasp(pickGeom, pickGeom->getPosition(),
                    -pickGeom->getOrientation().toRotationMatrix().col(1), 0.03);
                    controllerPara->setJawAngleChange(-5.0);
                    deviceClient->setButton(1, 1);
                #else 
                    //forma 1
                    auto centerSph = std::dynamic_pointer_cast<Sphere>(toolObj->getCollidingGeometry());
                    auto toolChange = std::make_shared<Sphere>(*centerSph);
                    toolChange->setRadius(centerSph->getRadius() * 5);
                    //LOG(INFO) << "PosX: "<< centerSph->getPosition()[0] << "PosY: "<< centerSph->getPosition()[1]<< "PosZ: "<< centerSph->getPosition()[2];
                    toolPicking->beginVertexGrasp(toolChange); //
                    pbdToolCollision->setEnabled(false);

                    auto m_geom = std::static_pointer_cast<TetrahedralMesh>(tissueObj->getPhysicsGeometry());
                    auto m_mesh = std::dynamic_pointer_cast<PointSet>(m_geom);

                    auto &p= m_mesh->getVertexPosition(int(toolPicking->getElementID()));
                    std::string message = 
                       "| (" + std::to_string(p[0])
                    + ", " + std::to_string(p[1])
                    + ", " + std::to_string(p[2]) + ")\n";

                    LOG(INFO)<< "FORCE: "<<controller->getDeviceForce().norm();

                    LOG(INFO)<< "Id elemet: "<<toolPicking->getElementID();
                    LOG(INFO)<< "PosVertexID: "<<message;
                    //auto controllerBall = toolObj->getComponent<ObjectControllerGhost>();
                    //auto visualBall = controllerBall->getGhostModel();
                    //auto geomBall = visualBall-> getGeometry(); 
                    //LOG(INFO) << "PosBX: "<< geomBall->getCenter()[0] << "PosBY: "<< geomBall->getCenter()[1]<< "PosBZ: "<< geomBall->getCenter()[2];
                
                #endif

                }
                else if (e->m_key == 's'){
                    /*const Vec3d pos = camController->getPosition();
                    const Quatd orientation = camController->getOrientation();
                    camController->setPosition(pos);
                    camController->setOrientation(orientation);
                    light->setPosition(pos);
                    light->setFocalPoint(pos - orientation.toRotationMatrix().col(2));*/
                }
                else if (e->m_key == 'i')
                {

                   /* auto tissueMesh = std::dynamic_pointer_cast<TetrahedralMesh>(tissueObj->getPhysicsGeometry());
                    auto toolGeom   = std::dynamic_pointer_cast<SurfaceMesh>(toolObjCut->getCollidingGeometry());

                    // Default config of the tool is pointing downwards on y
                    const Mat3d rot     = toolGeom->getRotation();
                    const Vec3d forward = (rot * Vec3d(0.0, 0.0, 0.1)).normalized();
                    const Vec3d left    = (rot * Vec3d(.1, 0.0, 0.0)).normalized();
                    const Vec3d n       = (rot * Vec3d(0.0, 1.0, 0.0)).normalized();

                    const Vec3d planePos        = toolGeom->getTranslation();
                    const Vec3d planeNormal     = n;
                    const double planeWidth     = 0.1; // Slightly larger than collision geometry
                    const double planeHalfWidth = planeWidth * 0.01;

                    std::shared_ptr<VecDataArray<double, 3>> tissueVerticesPtr = tissueMesh->getVertexPositions();
                    std::shared_ptr<VecDataArray<int, 4>> tissueIndicesPtr     = tissueMesh->getCells();
                    VecDataArray<double, 3>& tissueVertices = *tissueVerticesPtr;
                    VecDataArray<int, 4>& tissueIndices     = *tissueIndicesPtr;

                    // Compute which tets should be removed
                    std::unordered_set<int> removedTets;
                    for (int i = 0; i < tissueIndices.size(); i++)
                    {
                        Vec4i& tet = tissueIndices[i];
                        std::array<Vec3d, 4> tetVerts;
                        tetVerts[0] = tissueVertices[tet[0]];
                        tetVerts[1] = tissueVertices[tet[1]];
                        tetVerts[2] = tissueVertices[tet[2]];
                        tetVerts[3] = tissueVertices[tet[3]];

                        if (splitTest(tetVerts, planePos, left, planeHalfWidth, forward, planeHalfWidth, n))
                        {
                            cellRemoval->removeCellOnApply(i);
                        }
                    }
                    cellRemoval->apply();*/
                    // FORMA 1
                    
                    auto tissueMesh = std::dynamic_pointer_cast<TetrahedralMesh>(tissueObj->getPhysicsGeometry());
                    auto toolGeom   = std::dynamic_pointer_cast<SurfaceMesh>(toolObjCut->getCollidingGeometry());

                    // Default config of the tool is pointing downwards on y
                    const Mat3d rot     = toolGeom->getRotation();
                    const Vec3d forward = (rot * Vec3d(0.0, 0.0, 1.0)).normalized();
                    const Vec3d left    = (rot * Vec3d(1.0, 0.0, 0.0)).normalized();
                    const Vec3d n       = (rot * Vec3d(0.0, 1.0, 0.0)).normalized();

                    const Vec3d planePos        = toolGeom->getTranslation();
                    const Vec3d planeNormal     = n;
                    const double planeWidth     = 1.1; // Slightly larger than collision geometry
                    const double planeHalfWidth = planeWidth * 0.5;

                    std::shared_ptr<VecDataArray<double, 3>> tissueVerticesPtr = tissueMesh->getVertexPositions();
                    std::shared_ptr<VecDataArray<int, 4>> tissueIndicesPtr     = tissueMesh->getCells();
                    VecDataArray<double, 3>& tissueVertices = *tissueVerticesPtr;
                    VecDataArray<int, 4>& tissueIndices     = *tissueIndicesPtr;

                    //
                    std::unordered_set<int> removedTets;
                    for (int i = 0; i < tissueIndices.size(); i++)
                    {
                        Vec4i& tet = tissueIndices[i];
                        std::array<Vec3d, 4> tetVerts;
                        tetVerts[0] = tissueVertices[tet[0]];
                        tetVerts[1] = tissueVertices[tet[1]];
                        tetVerts[2] = tissueVertices[tet[2]];
                        tetVerts[3] = tissueVertices[tet[3]];

                        //plane intersections
                        if (splitTest(tetVerts, planePos, left, planeHalfWidth, forward, planeHalfWidth, n))
                        {
                            //containt tet element to will remove
                            removedTets.insert(i);
                        }
                    }

                    // Deal with diffs
                    std::shared_ptr<PbdConstraintContainer> constraintsPtr = tissueObj->getPbdModel()->getConstraints();
                    const std::vector<std::shared_ptr<PbdConstraint>>& constraints = constraintsPtr->getConstraints();

                    //
                    for (auto i : removedTets)
                    {
                        Vec4i& tet = tissueIndices[i];

                        //removed the contraint associate
                        for (auto j = constraints.begin(); j != constraints.end(); j++)
                        {
                            const std::vector<PbdParticleId>& vertexIds = (*j)->getParticles();
                            bool isSameTet = true;
                            for (int k = 0; k < 4; k++)
                            {
                                if (vertexIds[k].second != tet[k])
                                {
                                    isSameTet = false;
                                    break;
                                }
                            }
                            if (isSameTet)
                            {
                                constraintsPtr->eraseConstraint(j);
                                break;
                            }
                        }
                        tet = Vec4i(0, 0, 0, 0);
                    }

                    if (removedTets.size() > 0)
                    {
                        //// Update collision geometry by re-extracting the entire mesh
                        //auto map = std::dynamic_pointer_cast<PointwiseMap>(tissueObj->getPhysicsToCollidingMap());
                        //std::shared_ptr<SurfaceMesh> colMesh = tissueMesh->extractSurfaceMesh();
                        //map->setChildGeometry(colMesh);
                        //map->compute();
                        //map->update();
                        //colMesh->computeVertexNormals();
                        //tissueObj->setCollidingGeometry(colMesh);

                        tissueIndicesPtr->postModified();
                        tissueMesh->postModified();
                    }
                }
            });
        connect<KeyEvent>(viewer->getKeyboardDevice(), &KeyboardDeviceClient::keyRelease,
            [&](KeyEvent* e)
            {
                if (e->m_key == 'g')
                {

                #ifdef OPTION_GRASP
                    LOG(INFO) << "Jaw Opened!";
                    upperJawCollision->setEnabled(true);
                    lowerJawCollision->setEnabled(true);
                    jawPicking->endGrasp();
                    controllerPara->setJawAngleChange(5.0);
                #else
                    
                    toolPicking->endGrasp();
                    pbdToolCollision->setEnabled(true);
                    //toolObj->getComponent(id_ghost)

                    auto centerSph = std::dynamic_pointer_cast<Sphere>(toolObj->getCollidingGeometry());
                    auto toolChange = std::make_shared<Sphere>(*centerSph);
                    toolChange->setRadius(centerSph->getRadius() * 5);
                    LOG(INFO) << "PosX: "<< centerSph->getPosition()[0] << " PosY: "<< centerSph->getPosition()[1]<< " PosZ: "<< centerSph->getPosition()[2];
                    
                    auto controllerBall = toolObj->getComponent<ObjectControllerGhost>();
                    auto visualBall = controllerBall->getGhostModel();
                    auto geomBall = visualBall-> getGeometry(); 
                    LOG(INFO) << "PosBX: "<< geomBall->getCenter()[0] << " PosBY: "<< geomBall->getCenter()[1]<< " PosBZ: "<< geomBall->getCenter()[2];
                #endif

                }else if(e->m_key== '0'){ 
                    flag_rec = !flag_rec;
                    //foto
                    auto m_geom = std::static_pointer_cast<TetrahedralMesh>(tissueObj->getPhysicsGeometry());
                    m_mesh_coompared = std::dynamic_pointer_cast<PointSet>(m_geom);
                }else if (e->m_key == '1'){
                    flag_rec_vtkMesh = !flag_rec_vtkMesh;
                    timer.start();
                }else if (e->m_key == '2'){

                    flag_rec_vtkMesh_2 = !flag_rec_vtkMesh_2; 
                }


             });


    #ifdef OPTION_GRASP

        connect<Event>(controllerPara, &LaparoscopicToolController::JawClosed,
            [&](Event*)
            {
                LOG(INFO) << "Jaw Closed!";

                upperJawCollision->setEnabled(false);
                lowerJawCollision->setEnabled(false);
                jawPicking->beginCellGrasp(pickGeom, "SurfaceMeshToCapsuleCD");
            });
        connect<Event>(controllerPara, &LaparoscopicToolController::JawOpened,
            [&](Event*)
            {

                LOG(INFO) << "Jaw Opened!";

                upperJawCollision->setEnabled(true);
                lowerJawCollision->setEnabled(true);
                jawPicking->endGrasp();
            });
        #endif

        

        //active/deactive device to control
       // controller->setDevice(deviceClient);

        //show contact & intrument forces in viewer
        auto controllerForceTxt = mouseAndKeyControls->addComponent<ControllerForceText>();
        controllerForceTxt->setController(controller);
        controllerForceTxt->setCollision(pbdToolCollision);

        scene->addSceneObject(mouseAndKeyControls);

        connect<Event>(sceneManager, &SceneManager::preUpdate, [&](Event*)
            {
                //time desired to simulate in real time 
                pbdModel->getConfig()->m_dt = sceneManager->getDt();
            });

        driver->start();
    
    }

    return 0;
}
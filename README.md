# appSim-v

#license:
This code based in library: iMSTK, nlohmann_json, Vrpn
The iMSTK is distributed under the Apache License, Version 2.0.
The Nlohmann_json library is licensed under the MIT License.
The VRPN library is licensed under the Boost Software License 1.0
for more details see Readme.md.


#install 
git clone https://gitlab.com/mfermin/appsim-v.git
cd appSim-v
execute: ./compile


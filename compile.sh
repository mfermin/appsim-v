#!/bin/bash
echo "select imstk version ************"
echo "  1) imstk-v5"
echo "  2) imstk-v6"
echo "  3) imstk-master"

read n
case $n in
  1) echo " Compiling version appsim-release";version="imstk-v5";;
  2) echo " Compiling version appsim-release";version="imstk-v6";;
  3) echo " Compiling version appsim-release";version="imstk-master";;
  *) echo "invalid option";;
esac
echo $version

WHO=$(whoami)
echo "configuring user... "$WHO

if [ $WHO == "simanato" ]; then
	project="/home/simanato/workspace/iMSTKBox/iMSTK-simulator/appsim-dev"
elif [ $# -ge 23 ]; then
	project="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
else
  project="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
  echo "automatic configuration:" $WHO
fi

#Verify if exists build appsim
if [ -d "$project/build" ]; then
    # Take action if $DIR exists. #
    echo "Removing previos build in ${project}..."
    rm -r -f $project/build
    echo "Creating the build folder..."
    mkdir $project/build
else
    ###  Control will jump here if $DIR does NOT exists ###
    echo "Creating the build folder..."
    mkdir $project/build
fi

#Verify if exists build imstk & libs

#if [-d "$project/third_party" ]; then
  #echo "iMSTK detected"
#else 
#load env
#


cd $project/build
if [ -e "$project/env_appsim" ]; then
    echo "Loading env..."
    source "$project/env_appsim"
else
    ###  Control will jump here if $DIR does NOT exists ###
    echo "ERROR: $project/env_appsim version not configured "
fi


cmake ..
make 
